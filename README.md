# Rim4Py

This crappy library is used to pull information from RimWorld Def XML files, particularly for the purpose of generating translation data, although you're welcome to use it for other purposes.

## Compatibility

| RimWorld Version | Status |
|---:|---|
| 1.4 | WIP |
| 1.3 | WIP |
| Older | Not supported|

## Installing

This library is not yet ready for use.

## License

Copyright &copy;2022 Rob "N3X15" Nelson.

Available to you under the terms of the MIT Open-Source License Agreement.
