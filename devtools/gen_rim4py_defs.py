import asyncio
import python_minifier
import collections
import keyword
from io import StringIO
from pathlib import Path
from typing import Any, Callable, Dict, List, Optional, OrderedDict, Set, Tuple, Type

import black
import isort
from buildtools.indentation import IndentWriter
from ruamel.yaml import YAML as Yaml
from rich.progress import Progress

DEBUG: bool = True

YAML = Yaml(typ="rt", pure=True)

PATH_STRUCTS: Path = Path("data") / "def-structs"
PATH_PYDEFS: Path = Path("rim4py") / "defs"

FIELD_TYPES: Dict[str, Type["BaseDefField"]] = {}


class BaseDefField:
    TYPEID: str = ""
    TYPEDEF: str = ""
    IMPORTS: List[str] = []
    IS_TRANSLATABLE: bool = False

    def __init__(
        self,
        name: Optional[str] = None,
        optional: Optional[str] = None,
        default: Optional[Any] = None,
        help: Optional[List[str]] = None,
        xml_name: Optional[str] = None,
        translatable: Optional[bool] = None,
    ) -> None:
        self.name: str = name or ""
        self.optional: bool = optional or False
        self.default: Optional[Any] = default
        self.help: List[str] = help or []
        self.xml_name: str = xml_name or name or ""
        self.translatable: str = translatable
        self.root_type: bool = False

    def deserialize(self, k: str, data: Dict[str, Any]) -> None:
        self.name = k
        self.optional = not data.get("required", False)
        self.default = data.get("default")
        self.help = data.get("help")
        self.xml_name = data.get("xml-name") or self.name
        self.translatable = data.get("translatable")
        self.root_type = data.get("root-type", False)

    def generateImports(self, w: IndentWriter) -> None:
        """Spit out imports"""
        if self.translatable is None or self.translatable == True:
            w.writeline("import html")
        if self.optional:
            w.writeline("from typing import Optional")
        for l in self.IMPORTS:
            w.writeline(l)

    def generateInitCode(self, w: IndentWriter) -> None:
        """What to write in def __init__."""
        td = self.TYPEDEF
        if self.optional:
            td = f"Optional[{td}]"
        w.writeline(f"self.{self.name}: {td} = {self.default!r}")

    def generateFromDef(self, w: IndentWriter) -> None:
        """The actual Def XML deserialization code"""
        with w.writeline(
            f"if (ie := element.find({self.xml_name.casefold()!r})) is not None:"
        ):
            self.generateInnerDeserializeFromElement(w, "ie", f"self.{self.name}")
        with w.writeline(f"else:"):
            if self.optional:
                self.generateInnerAssignNull(w)
            else:
                w.writeline(
                    f"raise MissingElementException(tree.getpath(element), {self.xml_name.casefold()!r})"
                )

    def generateInnerDeserializeFromElement(
        self, w: IndentWriter, inner_element_var: str, to: str
    ) -> None:
        pass

    def generateInnerAssignNull(self, w: IndentWriter) -> None:
        w.writeline(f"self.{self.name} = None")

    def generateToDefElementTextCode(self, value_var: str) -> str:
        return f"str({value_var})"

    def shouldGenerateDefInject(self) -> bool:
        return (
            self.translatable is None and self.IS_TRANSLATABLE
        ) or self.translatable == True

    def generateToDefInjectable(
        self,
        w: IndentWriter,
        parent_element: str,
        prefix_str: Optional[str],
        value_var: str,
    ) -> bool:
        """<{defname}.{fieldname}>"""
        if self.shouldGenerateDefInject():
            value_var = self.generateToDefElementTextCode(value_var)
            defid: str = f"f'{{self.defName}}.{self.xml_name}'"
            if prefix_str is not None:
                defid = f"{prefix_str} + f'{{self.defName}}.{self.xml_name}'"
            contents: str = f"html.escape({value_var})"
            w.writeline(
                f"etree.SubElement({parent_element}, {defid}, {{}}).text = {contents}"
            )
            return True
        return False


class DefStrField(BaseDefField):
    TYPEID = "str"
    TYPEDEF = "str"
    IMPORTS = []
    IS_TRANSLATABLE = True

    def generateInnerDeserializeFromElement(
        self, w: IndentWriter, inner_element_var: str, to: str
    ) -> None:
        w.writeline(f"{to} = {inner_element_var}.text")


FIELD_TYPES[DefStrField.TYPEID] = DefStrField


class DefIntField(BaseDefField):
    TYPEID = "int"
    TYPEDEF = "int"
    IMPORTS = []

    def generateInnerDeserializeFromElement(
        self, w: IndentWriter, inner_element_var: str, to: str
    ) -> None:
        w.writeline(f"{to} = int({inner_element_var}.text)")


FIELD_TYPES[DefIntField.TYPEID] = DefIntField


class DefBoolField(BaseDefField):
    TYPEID = "bool"
    TYPEDEF = "bool"
    IMPORTS = []

    def generateInnerDeserializeFromElement(
        self, w: IndentWriter, inner_element_var: str, to: str
    ) -> None:
        w.writeline(f"{to} = bool({inner_element_var}.text)")


FIELD_TYPES[DefBoolField.TYPEID] = DefBoolField


class DefFloatField(BaseDefField):
    TYPEID = "float"
    TYPEDEF = "float"
    IMPORTS = []

    def generateInnerDeserializeFromElement(
        self, w: IndentWriter, inner_element_var: str, to: str
    ) -> None:
        w.writeline(f"{to} = float({inner_element_var}.text)")


FIELD_TYPES[DefFloatField.TYPEID] = DefFloatField


class DefSubtypeField(BaseDefField):
    TYPEID = "def"
    TYPEDEF = "Def"
    IMPORTS = []

    def __init__(
        self,
        name: Optional[str] = None,
        optional: Optional[str] = None,
        default: Optional[Any] = None,
        help: Optional[List[str]] = None,
        xml_name: Optional[str] = None,
        translatable: Optional[bool] = None,
    ) -> None:
        super().__init__(name, optional, default, help, xml_name, translatable)
        self.module: str = ""
        self.clsname: str = ""

    def deserialize(self, k: str, data: Dict[str, Any]) -> None:
        super().deserialize(k, data)
        self.module = data["module"]
        self.clsname = data["class"]

    def generateImports(self, w: IndentWriter) -> None:
        super().generateImports(w)
        # w.writeline(f'from .def_ import Def')
        if self.module is not None:
            w.writeline(f"from {self.module} import {self.clsname}")

    def generateInnerDeserializeFromElement(
        self, w: IndentWriter, inner_element_var: str, to: str
    ) -> None:
        w.writeline(f"{to} = {self.clsname}()")
        w.writeline(f"{to}.fromDefElement(tree, {inner_element_var})")

    def generateToDefInjectable(
        self,
        w: IndentWriter,
        parent_element: str,
        prefix_str: Optional[str],
        value_var: str,
    ) -> bool:
        # return super().generateToDefInjectable(w, parent_element, prefix_str)
        if self.shouldGenerateDefInject():
            w.writeline(f"{value_var}.toDefInjectElement(tree, {parent_element})")
            return True
        return False


FIELD_TYPES[DefSubtypeField.TYPEID] = DefSubtypeField


class DefListField(BaseDefField):
    TYPEID = "list"
    TYPEDEF = "List"
    IMPORTS = ["from typing import List"]

    def __init__(
        self,
        name: Optional[str] = None,
        optional: Optional[str] = None,
        default: Optional[Any] = None,
        help: Optional[List[str]] = None,
        xml_name: Optional[str] = None,
        translatable: Optional[bool] = None,
        subtype: BaseDefField = None,
    ) -> None:
        super().__init__(name, optional, default, help, xml_name, translatable)
        self.subtype: BaseDefField = subtype
        self.list_elem_name: Optional[str] = "li"

    def generateImports(self, w: IndentWriter) -> None:
        super().generateImports(w)
        self.subtype.generateImports(w)

    def generateInitCode(self, w: IndentWriter) -> None:
        td = f"{self.TYPEDEF}[{self.subtype.TYPEDEF}]"
        if self.optional:
            td = f"Optional[{td}]"
        w.writeline(f"self.{self.name}: {td} = {self.default!r}")

    def deserialize(self, k: str, data: Dict[str, Any]) -> None:
        super().deserialize(k, data)
        self.list_elem_name = data.get("list-elem-name", "li")
        v = data["subtype"]
        self.subtype = FIELD_TYPES[v["type"]]()
        self.subtype.deserialize(self.name, v)

    def generateInnerDeserializeFromElement(
        self, w: IndentWriter, inner_element_var: str, to: str
    ) -> None:
        if self.list_elem_name is not None:
            with w.writeline(
                f"for le in {inner_element_var}.findall({self.list_elem_name.casefold()!r}):"
            ):
                self.subtype.generateInnerDeserializeFromElement(w, "le", "leo")
                w.writeline(f"{to}.append(leo)")
        else:
            with w.writeline(f"for li in {inner_element_var}:"):
                self.subtype.generateInnerDeserializeFromElement(w, "li", "leo")
                w.writeline(f"{to}.append(leo)")

    def generateToDefInjectable(
        self,
        w: IndentWriter,
        parent_element: str,
        prefix_str: Optional[str],
        value_str: str,
    ) -> bool:
        if self.shouldGenerateDefInject():
            with w.writeline(f"for i, li in enumerate({value_str}):"):
                if self.list_elem_name is not None:
                    w.writeline(
                        f"lie = etree.SubElement({parent_element}, {self.list_elem_name.casefold()!r}, {{}})"
                    )
                    self.subtype.generateToDefInjectable(
                        w, "lie", f"{self.name}.{{i}}", f"{value_str}[i]"
                    )
                else:
                    self.subtype.generateToDefInjectable(
                        w, "li", f"{self.name}.{{i}}", f"{value_str}[i]"
                    )
            return True
        return False


FIELD_TYPES[DefListField.TYPEID] = DefListField


class DefMeta:
    def __init__(self) -> None:
        self.type: str = ""
        self.basecls: Optional[str] = "Def"
        self.imports: List[str] = ["from .def_ import Def"]
        self.fields: OrderedDict[str, BaseDefField] = collections.OrderedDict()

    def fromYAML(self, data: Dict[str, Any]) -> None:
        self.type = data["type"]
        if "base" in data:
            # print(__file__, 228, repr(data['base']))
            if data["base"] is None:
                self.basecls = None
                self.imports = []
            else:
                basedata = data["base"]
                self.basecls = basedata["imports"][0]
                module = basedata["module"]
                if module is not None:
                    self.imports = [f"from {module} import {self.basecls}"]
        if "imports" in data:
            for k, v in data["imports"].items():
                vs = ",".join(v)
                if len(v) > 1:
                    vs = f"({vs})"
                self.imports.append(f"from {k} import {vs}")

        for k, v in data["fields"].items():
            fld = FIELD_TYPES[v["type"]]()
            fld.deserialize(k, v)
            self.fields[fld.name] = fld

    def toPython(self, w: IndentWriter) -> None:
        # w.writeline('# @generated by gen_rim4py_defs.py - DO NOT EDIT BY HAND')
        # w.writeline('import html')
        w.writeline("from lxml import etree")

        for imp in sorted(set(self.imports)):
            w.writeline(imp)
        [x.generateImports(w) for x in self.fields.values()]
        w.writeline(f"__all__=[{self.type!r}]")
        baseclassdecl = ""
        if self.basecls is not None:
            baseclassdecl = f"({self.basecls})"
        with w.writeline(f"class {self.type}{baseclassdecl}:"):
            w.writeline(f"TAG: str = {self.type!r}")
            with w.writeline(f"def __init__(self) -> None:"):
                if self.basecls is not None:
                    w.writeline("super().__init__()")
                [x.generateInitCode(w) for x in self.fields.values()]

            with w.writeline(
                f"def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:"
            ):
                if self.basecls is not None:
                    w.writeline("super().fromDefElement(tree, element)")
                [x.generateFromDef(w) for x in self.fields.values()]

            # with w.writeline(f'def toDefElement(self, tree: etree._ElementTree, parent: etree._Element) -> None:'):
            #     if self.basecls is not None:
            #         w.writeline('el = super().toDefElement(tree, parent)')
            #     [x.generateToDef(w) for x in self.fields.values()]

            with w.writeline(
                f"def toDefInjectElement(self, tree: etree._ElementTree, element: etree._Element) -> None:"
            ):
                if self.basecls is not None:
                    w.writeline("super().toDefInjectElement(tree, element)")
                [
                    x.generateToDefInjectable(w, "element", None, f"self.{x.name}")
                    for x in self.fields.values()
                ]
                w.writeline("return")


async def generateForVersion(prog: Progress, major: int, minor: int) -> None:
    version_dot: str = f"{major}.{minor}"
    version_pymod: str = f"v{major}_{minor}"
    rootdir = PATH_PYDEFS / version_pymod
    defs: Dict[str, DefMeta] = {}
    writtenFiles: Set[Path] = set()
    existingFiles: Set[Path] = set(map(lambda x: x.absolute(), rootdir.glob("*.py")))
    writtenFiles.add((rootdir / "exceptions.py").absolute())

    def processYAMLFile(path: Path) -> None:
        with path.open("r") as f:
            data = YAML.load(f)
        def_ = DefMeta()
        def_.fromYAML(data)
        def_.path = path
        defs[def_.type] = def_

    def processEachFileWithProgress(
        dir: Path,
        pattern: str,
        cb: Callable[[Path], None],
        desc: str,
        eachfiledesc: Optional[str] = None,
    ) -> None:
        allf = list(dir.rglob(pattern))
        l = len(allf)
        for path in prog.track(allf, description=desc.format(dir=dir),total=l):
            if eachfiledesc:
                prog.print(eachfiledesc.format(path=path))
            cb(path)

    processEachFileWithProgress(
        Path("data") / version_dot / "defs-auto",
        "*.yml",
        processYAMLFile,
        desc="Reading files in [cyan]{dir}[/cyan]",
        #eachfiledesc="[cyan]Processing [light yellow bold]{path}[/light yellow bold]...",
    )
    processEachFileWithProgress(
        Path("data") / version_dot / "defs",
        "*.yml",
        processYAMLFile,
        desc="Reading files in [cyan]{dir}[/cyan]",
        #eachfiledesc="[cyan]Processing [light yellow bold]{path}[/light yellow bold]...",
    )

    writtenDefs: List[Tuple[str, str]] = []
    for def_ in prog.track(defs.values(), description='Generating code...'):
        #prog.print(f"{def_.path}...", end="")
        intermediate = ""
        with StringIO() as f:
            w = IndentWriter(f)
            def_.toPython(w)
            intermediate = f.getvalue()
        intermediate = isort.code(intermediate)
        # print(intermediate)
        try:
            intermediate = black.format_str(intermediate, mode=black.Mode())
        except Exception as e:
            print(intermediate)
            raise e
        fn = def_.path.stem.lower()
        if keyword.iskeyword(fn):
            fn += "_"
        newpath = rootdir / f"{fn}.py"
        srcfile = def_.path.absolute().relative_to(Path.cwd())
        genfile = Path(__file__).relative_to(Path.cwd())
        newpath.parent.mkdir(parents=True,exist_ok=True)
        with newpath.open("w") as f:
            f.write(f"# @generated by {genfile} - DO NOT EDIT BY HAND\n")
            f.write(f"# Generated from {srcfile}\n")
            f.write("from ..exceptions import MissingElementException\n")
            if DEBUG:
                f.write(intermediate)
            else:
                f.write(
                    python_minifier.minify(
                        intermediate, str(newpath), remove_annotations=False
                    )
                )
        writtenDefs.append((fn, def_.type))
        writtenFiles.add(newpath.absolute())
        #print(" DONE!")
    o: str = """from __future__ import annotations
from typing import Dict, Type
"""

    data = {}
    for stem, clsname in sorted(writtenDefs):
        if stem is not None:
            o += f"from .{stem.lower()} import {clsname}\n"
        data[stem] = clsname

    o += "DEF_TYPES: Dict[str, Type[Def]] = {"
    clsname: str
    for filename, clsname in sorted(data.items()):
        o += f"{clsname.casefold()!r}: {clsname},"
    o += "}\n"
    o = isort.code(o)
    o = black.format_str(o, mode=black.Mode())
    newfile = rootdir / "__init__.py"
    with newfile.open("w") as f:
        f.write(o)
    writtenFiles.add(newfile.absolute())
    print(f"{newfile}... DONE!")

    to_delete = existingFiles - writtenFiles
    new_files = writtenFiles - existingFiles

    def announce_and_delete(path: Path):
        #print(f"- {path.relative_to(Path.cwd())}")
        path.unlink()

    [announce_and_delete(x) for x in sorted(to_delete)]
    #[print(f"+ {x.relative_to(Path.cwd())}") for x in sorted(new_files)]


async def main() -> None:
    with Progress() as progress:
        await generateForVersion(progress, 1, 3)
        await generateForVersion(progress, 1, 4)
    print("Done")


if __name__ == "__main__":
    asyncio.run(main())
