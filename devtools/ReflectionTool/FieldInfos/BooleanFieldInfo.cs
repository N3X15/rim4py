using dnlib.DotNet;

namespace ReflectionTool
{
    public class BooleanFieldInfo : FieldInfo
    {
        public bool? initialValue = null;
        public BooleanFieldInfo() : base(EFieldType.BOOL)
        {
        }

        public override void FromFieldDef(FieldDef f)
        {
            base.FromFieldDef(f);
            if(f.HasConstant)
                initialValue = (bool)f.Constant.Value;
            if(f.HasAttribute("DefaultAttribute"))
                initialValue = (bool)f.CustomAttributes.Find("DefaultAttribute").ConstructorArguments[0].Value;
        }

        public override Dictionary<string, object> Serialize()
        {
            var o = base.Serialize();
            if(initialValue != null)
                o.Add("default", (object)initialValue);
            return o;
        }
    }
}