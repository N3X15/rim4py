using dnlib.DotNet;

namespace ReflectionTool
{
    public class FloatFieldInfo : FieldInfo
    {
        public float? initialValue = null;
        public FloatFieldInfo() : base(EFieldType.FLOAT)
        {
        }

        public override void FromFieldDef(FieldDef f)
        {
            base.FromFieldDef(f);
            if(f.HasConstant)
                initialValue = (float)f.Constant.Value;
            if(f.HasAttribute("DefaultAttribute"))
                initialValue = (float)f.CustomAttributes.Find("DefaultAttribute").ConstructorArguments[0].Value;
        }

        public override Dictionary<string, object> Serialize()
        {
            var o = base.Serialize();
            if(initialValue != null)
                o.Add("default", (object)initialValue);
            return o;
        }
    }
}