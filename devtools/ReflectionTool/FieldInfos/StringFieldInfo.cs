using dnlib.DotNet;

namespace ReflectionTool
{
    public class StringFieldInfo : FieldInfo
    {
        public string? initialValue = null;
        public StringFieldInfo() : base(EFieldType.STR)
        {
        }

        public override void FromFieldDef(FieldDef f)
        {
            base.FromFieldDef(f);
            if(f.HasConstant)
                initialValue = (string)f.Constant.Value;
            if(f.HasAttribute("DefaultAttribute"))
                initialValue = (string)f.CustomAttributes.Find("DefaultAttribute").ConstructorArguments[0].Value;
        }

        public override Dictionary<string, object> Serialize()
        {
            var o = base.Serialize();
            if(initialValue != null)
                o.Add("default", (object)initialValue);
            return o;
        }
    }
}