using dnlib.DotNet;

namespace ReflectionTool
{
    public class DefFieldInfo : FieldInfo
    {
        public TypeSig? defType = null;
        public DefFieldInfo() : base(EFieldType.DEF)
        {
        }

        public override void FromFieldDef(FieldDef f)
        {
            base.FromFieldDef(f);
            defType = f.FieldType;
        }

        public override Dictionary<string, object> Serialize()
        {
            var o = base.Serialize();
            if (defType is null)
                throw new NullReferenceException();
            o.Add("module", "." + defType.TypeName.ToLower());
            o.Add("class", defType.TypeName);
            // if (defType.IsGenericInstanceType){
            //     var gensig = defType.ToGenericInstSig();
            //     o.Add("generic", gensig.GenericType.TypeName);
            //     gensig.GenericType.
            // }
            return o;
        }
    }
}