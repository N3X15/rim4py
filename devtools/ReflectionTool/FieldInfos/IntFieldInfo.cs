using dnlib.DotNet;

namespace ReflectionTool
{
    public class IntFieldInfo : FieldInfo
    {
        public int? initialValue = null;
        public IntFieldInfo() : base(EFieldType.INT)
        {
        }

        public override void FromFieldDef(FieldDef f)
        {
            base.FromFieldDef(f);
            if(f.HasConstant)
                initialValue = (int)f.Constant.Value;
            if(f.HasAttribute("DefaultAttribute"))
                initialValue = (int)f.CustomAttributes.Find("DefaultAttribute").ConstructorArguments[0].Value;
        }

        public override Dictionary<string, object> Serialize()
        {
            var o = base.Serialize();
            if(initialValue != null)
                o.Add("default", (object)initialValue);
            return o;
        }
    }
}