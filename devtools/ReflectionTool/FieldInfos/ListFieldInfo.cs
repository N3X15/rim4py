using dnlib.DotNet;

namespace ReflectionTool
{
    public class ListFieldInfo : FieldInfo
    {
        public FieldInfo? value;
        public ListFieldInfo() : base(EFieldType.LIST)
        {
        }

        public override void FromFieldDef(FieldDef f)
        {
            base.FromFieldDef(f);
        }

        public override Dictionary<string, object> Serialize()
        {
            var o = base.Serialize();
            if(value is null){
                throw new ArgumentNullException("value");
            }
            o["value"] = value.Serialize();
            return o;
        }
    }
}