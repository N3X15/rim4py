using dnlib.DotNet;

namespace ReflectionTool
{
    public class DictFieldInfo : FieldInfo
    {
        public FieldInfo? key;
        public FieldInfo? value;
        public DictFieldInfo() : base(EFieldType.DICT)
        {
        }

        public override void FromFieldDef(FieldDef f)
        {
            base.FromFieldDef(f);
        }

        public override Dictionary<string, object> Serialize()
        {
            var o = base.Serialize();
            if (key is null)
            {
                throw new ArgumentNullException("key");
            }
            if (value is null)
            {
                throw new ArgumentNullException("value");
            }
            o["key"] = key.Serialize();
            o["value"] = value.Serialize();
            return o;
        }
    }
}