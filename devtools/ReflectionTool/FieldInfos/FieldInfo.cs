using dnlib.DotNet;

namespace ReflectionTool
{
    public enum EFieldType
    {
        STR = 0,
        INT,
        FLOAT,
        DEF,
        BOOL,
        DICT,
        LIST,
    }

    [Flags]
    public enum EFieldFlags
    {
        NONE = 0,
        UNSAVED,
        MUSTTRANSLATE,
        NOTRANSLATE,
    }

    public class FieldInfo
    {
        public string Name = "";
        public EFieldType Type;
        public EFieldFlags Flags = EFieldFlags.NONE;

        public bool isUnsaved = false;
        public bool mustTranslate = false;
        public bool noTranslate = false;

        public FieldInfo(EFieldType t)
        {
            this.Type = t;
        }

        public virtual void FromFieldDef(FieldDef f)
        {
            this.Name = f.Name;
            this.Flags = EFieldFlags.NONE;
            if (f.HasAttribute("UnsavedAttribute"))
            {
                this.Flags |= EFieldFlags.UNSAVED; isUnsaved = true;
            }
            if (f.HasAttribute("MustTranslateAttribute"))
            {
                this.Flags |= EFieldFlags.MUSTTRANSLATE; mustTranslate = true;
            }
            if (f.HasAttribute("NoTranslateAttribute"))
            {
                this.Flags |= EFieldFlags.NOTRANSLATE; noTranslate = true;
            }
        }

        public bool IsTranslatable
        {
            get
            {
                return !this.Flags.HasFlag(EFieldFlags.UNSAVED) && !this.Flags.HasFlag(EFieldFlags.NOTRANSLATE);
            }
        }

        public virtual Dictionary<string, object> Serialize()
        {
            List<string> flags = new List<string>();
            foreach (var flag in Enum.GetValues<EFieldFlags>())
                if (this.Flags.HasFlag(flag) && flag != EFieldFlags.NONE)
                    flags.Add(flag.ToString());
            var o = new Dictionary<string, object>(){
                //{"name", this.Name},
                {"type", this.Type.ToString().ToLower()},
                {"flags", flags},
            };
            if (!IsTranslatable)
                o["translatable"] = false;
            return o;
        }
    }
}