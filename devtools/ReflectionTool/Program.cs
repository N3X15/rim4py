﻿using dnlib.DotNet;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace ReflectionTool
{
    public class Program
    {
        const int FILE_VERSION = 0;
        private static string OutputPath = "";
        private static TypeDef? IEnumerableSig = null;

        static bool HasNoTranslate(FieldDef fld)
        {
            if (!fld.HasCustomAttributes)
                return false;
            return fld.CustomAttributes.FindAll("NoTranslate").Count() > 0;
        }
        public static void Main(string[] args)
        {
            //Console.WriteLine(string.Join(",",args));
            if (args == null || args.Count() < 2)
            {
                Console.WriteLine("ReflectionTool.exe <path\\to\\Assembly-CSharp.dll> <output\\dir>");
                return;
            }
            string asm = args[0];
            OutputPath = args[1];

            var mod = ModuleDefMD.Load(asm);
            var corlib = ModuleDefMD.Load(typeof(void).Module.FullyQualifiedName);
            IEnumerableSig = corlib.GetTypes().First(x => x.Name == "IEnumerable");

            var allTypes = mod.GetTypes();
            var baseDef = allTypes.First(x => x.Name == "Def");
            ForEachDef(mod, baseDef);
            allTypes.Where(x => (!x.Equals(baseDef)) && x.BaseType != null && x.BaseType.Equals(baseDef)).ToList().ForEach(t => ForEachDef(mod, t));

            var def = ProcessType(mod, allTypes.Where(x => x.Name.Equals("ModMetaDataInternal")).First());
            def.fields.ForEach(x =>
            {
                x.Flags = EFieldFlags.NOTRANSLATE;
            });
            DumpTo(def, Path.Combine(OutputPath, $"{def.name}.yml"));

        }

        private static DefInfo ProcessType(ModuleDefMD mod, TypeDef typ)
        {

            DefInfo def = new DefInfo();
            def.name = typ.Name;
            FieldInfo? fi;
            foreach (var fld in typ.Fields.Where(f => !f.IsStatic && f.IsPublic/* && !f.FieldType.IsGenericInstanceType*/))
            {
                if ((fi = ProcessTypeSig(mod, fld, fld.FieldType)) != null)
                    def.fields.Add(fi);
            }
            return def;
        }

        private static FieldInfo? ProcessTypeSig(ModuleDef mod, FieldDef fld, TypeSig typeSig)
        {
            FieldInfo? fi = null;
            bool notranslate = false;

            if (mod.CorLibTypes.String.Equals(typeSig))
            {
                fi = new StringFieldInfo();
            }
            else if (mod.CorLibTypes.Int32.Equals(typeSig))
            {
                fi = new IntFieldInfo();
            }
            else if (mod.CorLibTypes.Single.Equals(typeSig))
            {
                fi = new FloatFieldInfo();
            }
            else if (mod.CorLibTypes.Boolean.Equals(typeSig))
            {
                fi = new BooleanFieldInfo();
            }
            else if (typeSig.TypeName == "Type")
            {
                fi = new StringFieldInfo();
                notranslate = true;
            }
            else if (typeSig.IsGenericInstanceType)
            {
                var gis = typeSig.ToGenericInstSig();
                switch(gis.GetName()) {
                    case "Dictionary": 
                        fi = new DictFieldInfo();
                        ((DictFieldInfo)fi).key = ProcessTypeSig(mod, fld, gis.GenericArguments[0]);
                        ((DictFieldInfo)fi).value = ProcessTypeSig(mod, fld, gis.GenericArguments[1]);
                        return fi;
                    case "List":
                        fi = new ListFieldInfo();
                        ((ListFieldInfo)fi).value = ProcessTypeSig(mod, fld, gis.GenericArguments[0]);
                        return fi;
                }
            }
            else if (IsDictionary(typeSig))
            {
                return null; // Not supported.
            }
            // TODO: Make EnumerableFieldInfo
            else if (IsEnumerable(typeSig))
            {
                //fi = new EnumerableFieldInfo();
            }
            else
            {
                fi = new DefFieldInfo();
            }
            if (fi == null)
                return null;
            fi.FromFieldDef(fld);
            if (notranslate)
                fi.Flags |= EFieldFlags.NOTRANSLATE;
            return fi;
        }

        private static void ForEachDef(ModuleDefMD mod, TypeDef typ)
        {
            DefInfo def = ProcessType(mod, typ);
            DumpTo(def, Path.Combine(OutputPath, $"{def.name}.yml"));
        }
        private static void DumpTo(DefInfo def, string newpath)
        {

            var serializer = new SerializerBuilder().WithNamingConvention(CamelCaseNamingConvention.Instance).Build();
            if (!Directory.Exists(OutputPath))
                Directory.CreateDirectory(OutputPath);
            File.WriteAllText(newpath, serializer.Serialize(def.Serialize()));
            Console.WriteLine($"Dumped {def.name} -> {newpath}");
        }

        private static bool IsEnumerable(TypeSig fld)
        {
            switch (GetBaseName(fld))
            {
                case "List`1":
                case "HashSet`1":
                case "IEnumerable`1":
                    return true;
            }
            return false;
        }

        private static bool IsDictionary(TypeSig fld)
        {
            switch (GetBaseName(fld))
            {
                case "Dictionary`2":
                    return true;
            }
            return false;
        }

        private static string GetBaseName(TypeSig sig)
        {
            if (sig.IsGenericInstanceType)
            {
                var gsig = sig.ToGenericInstSig();
                return gsig.GenericType.TypeName;
            }
            return sig.TypeName;
        }

        // Based loosely on DefInjectionUtility.FieldsInDeterministicOrder().
        private static IEnumerable<FieldDef> SortDeterministically(IEnumerable<FieldDef> flds)
        {
            return from x in flds
                   orderby
                x.HasAttribute("UnsafeAttribute") || x.HasAttribute("NoTranslateAttribute"),
                x.Name == "label" descending,
                x.Name == "description" descending,
                x.Name
                   select x;
        }
    }
}