namespace ReflectionTool
{
    public class DefInfo
    {
        public string name = "";
        public List<FieldInfo> fields = new List<FieldInfo>();

        public DefInfo() { }

        public Dictionary<string, object> Serialize()
        {
            var fldDict = new Dictionary<string, object>();
            foreach (var fld in fields)
            {
                fldDict[fld.Name] = fld.Serialize();
            }
            return new Dictionary<string, object>(){
                {"type", name},
                {"fields", fldDict},
            };
        }
    }
}