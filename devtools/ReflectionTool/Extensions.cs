using dnlib.DotNet;

namespace ReflectionTool
{
    static class Extensions
    {
        public static bool HasAttribute(this FieldDef field, string fullName)
        {
            if (!field.HasCustomAttributes)
                return false;
            return field.CustomAttributes.FirstOrDefault(x => x.AttributeType.Name.Equals(fullName)) != null;
        }
    }
}