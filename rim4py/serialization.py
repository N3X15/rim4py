from pathlib import Path
from typing import BinaryIO, Optional, Type
from lxml import etree, html

from logging import getLogger

log = getLogger(__name__)

XML_TRUE: str = "true".casefold()
XML_FALSE: str = "false".casefold()
XML_ISNULL: str = "isnull".casefold()

def treatTreeForParsing(tree: etree._ElementTree) -> None:
    el: etree._Element
    for el in tree.iter():
        el.tag = el.tag.casefold()  # Like str.lower(), but prescription-strength.
        el.attrib = {k.casefold(): v for k, v in el.attrib.items()}


class RimSerializable:
    def __init__(self) -> None:
        pass

    def toXML(self, parent: etree._Element) -> None:
        pass

    def fromXML(self, parent: etree._Element) -> None:
        pass


class RimXMLDeserializer:
    """
    Yes, we really have to do this.

    RimWorld handles XML in a case-insensitive manner, but W3C specifies XML as case-sensitive,
    so we have to wrap lxml in a bunch of additional bullshit to fix it.

    Sigh.
    """

    @classmethod
    def LoadFromFile(cls, type: Type[RimSerializable], file: Path) -> RimSerializable:
        with file.open("rb") as f:
            return cls.LoadFromHandle(type, f, str(file))

    @classmethod
    def LoadFromHandle(
        cls, type: Type[RimSerializable], f: BinaryIO, filename: Optional[str] = None
    ) -> Optional[RimSerializable]:
        # parser = html.HTMLParser(encoding='utf-8', collect_ids=False, huge_tree=True,no_network=True,remove_blank_text=True)
        tree: etree._ElementTree
        try:
            tree = etree.parse(f)
        except Exception as e:
            log.exception(e, extra={"filename": filename})
        return cls.LoadFromTree(type, tree, filename)

    @classmethod
    def LoadFromTree(
        cls,
        type: Type[RimSerializable],
        tree: etree._ElementTree,
        filename: Optional[str] = None,
    ) -> Optional[RimSerializable]:
        treatTreeForParsing(tree)
        cls.LoadFromElement(type, tree.getroot(), filename)

    @classmethod
    def LoadFromElement(
        cls,
        type: Type[RimSerializable],
        el: etree._Element,
        filename: Optional[str] = None,
    ) -> Optional[RimSerializable]:
        if el.get(XML_ISNULL).casefold() == XML_TRUE:
            return None
        ser: RimSerializable = type()
        ser.fromXML()
        return ser
