from typing import Any, List, Optional

from lxml import etree

__all__ = ['MissingElementException']

class MissingElementException(Exception):
    def __init__(self, *args: object) -> None:
        super().__init__(*args)