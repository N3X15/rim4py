# @generated by devtools\gen_rim4py_defs.py - DO NOT EDIT BY HAND
# Generated from data\1.4\defs-auto\StatDef.yml
from ..exceptions import MissingElementException
import html
from typing import Optional

from lxml import etree

from .def_ import Def
from .developmentalstage import DevelopmentalStage
from .simplecurve import SimpleCurve
from .skilldef import SkillDef
from .statcategorydef import StatCategoryDef
from .tostringnumbersense import ToStringNumberSense
from .tostringstyle import ToStringStyle

__all__ = ["StatDef"]


class StatDef(Def):
    TAG: str = "StatDef"

    def __init__(self) -> None:
        super().__init__()
        self.category: Optional[Def] = None
        self.workerClass: Optional[str] = None
        self.labelForFullStatList: Optional[str] = None
        self.forInformationOnly: Optional[bool] = None
        self.hideAtValue: Optional[float] = None
        self.alwaysHide: Optional[bool] = None
        self.showNonAbstract: Optional[bool] = None
        self.showIfUndefined: Optional[bool] = None
        self.showOnPawns: Optional[bool] = None
        self.showOnHumanlikes: Optional[bool] = None
        self.showOnNonWildManHumanlikes: Optional[bool] = None
        self.showOnAnimals: Optional[bool] = None
        self.showOnMechanoids: Optional[bool] = None
        self.showOnNonWorkTables: Optional[bool] = None
        self.showOnDefaultValue: Optional[bool] = None
        self.showOnUnhaulables: Optional[bool] = None
        self.showOnUntradeables: Optional[bool] = None
        self.neverDisabled: Optional[bool] = None
        self.showZeroBaseValue: Optional[bool] = None
        self.showOnSlavesOnly: Optional[bool] = None
        self.showOnPlayerMechanoids: Optional[bool] = None
        self.showDevelopmentalStageFilter: Optional[Def] = None
        self.hideInClassicMode: Optional[bool] = None
        self.displayPriorityInCategory: Optional[int] = None
        self.toStringNumberSense: Optional[Def] = None
        self.toStringStyle: Optional[Def] = None
        self.formatString: Optional[str] = None
        self.formatStringUnfinalized: Optional[str] = None
        self.finalizeEquippedStatOffset: Optional[bool] = None
        self.statFactorsExplanationHeader: Optional[str] = None
        self.defaultBaseValue: Optional[float] = None
        self.noSkillOffset: Optional[float] = None
        self.applyFactorsIfNegative: Optional[bool] = None
        self.noSkillFactor: Optional[float] = None
        self.postProcessCurve: Optional[Def] = None
        self.minValue: Optional[float] = None
        self.maxValue: Optional[float] = None
        self.valueIfMissing: Optional[float] = None
        self.roundValue: Optional[bool] = None
        self.roundToFiveOver: Optional[float] = None
        self.minifiedThingInherits: Optional[bool] = None
        self.supressDisabledError: Optional[bool] = None
        self.cacheable: Optional[bool] = None
        self.scenarioRandomizable: Optional[bool] = None
        self.disableIfSkillDisabled: Optional[Def] = None
        self.immutable: Optional[bool] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        super().fromDefElement(tree, element)
        if (ie := element.find("category")) is not None:
            self.category = StatCategoryDef()
            self.category.fromDefElement(tree, ie)
        else:
            self.category = None
        if (ie := element.find("workerclass")) is not None:
            self.workerClass = ie.text
        else:
            self.workerClass = None
        if (ie := element.find("labelforfullstatlist")) is not None:
            self.labelForFullStatList = ie.text
        else:
            self.labelForFullStatList = None
        if (ie := element.find("forinformationonly")) is not None:
            self.forInformationOnly = bool(ie.text)
        else:
            self.forInformationOnly = None
        if (ie := element.find("hideatvalue")) is not None:
            self.hideAtValue = float(ie.text)
        else:
            self.hideAtValue = None
        if (ie := element.find("alwayshide")) is not None:
            self.alwaysHide = bool(ie.text)
        else:
            self.alwaysHide = None
        if (ie := element.find("shownonabstract")) is not None:
            self.showNonAbstract = bool(ie.text)
        else:
            self.showNonAbstract = None
        if (ie := element.find("showifundefined")) is not None:
            self.showIfUndefined = bool(ie.text)
        else:
            self.showIfUndefined = None
        if (ie := element.find("showonpawns")) is not None:
            self.showOnPawns = bool(ie.text)
        else:
            self.showOnPawns = None
        if (ie := element.find("showonhumanlikes")) is not None:
            self.showOnHumanlikes = bool(ie.text)
        else:
            self.showOnHumanlikes = None
        if (ie := element.find("showonnonwildmanhumanlikes")) is not None:
            self.showOnNonWildManHumanlikes = bool(ie.text)
        else:
            self.showOnNonWildManHumanlikes = None
        if (ie := element.find("showonanimals")) is not None:
            self.showOnAnimals = bool(ie.text)
        else:
            self.showOnAnimals = None
        if (ie := element.find("showonmechanoids")) is not None:
            self.showOnMechanoids = bool(ie.text)
        else:
            self.showOnMechanoids = None
        if (ie := element.find("showonnonworktables")) is not None:
            self.showOnNonWorkTables = bool(ie.text)
        else:
            self.showOnNonWorkTables = None
        if (ie := element.find("showondefaultvalue")) is not None:
            self.showOnDefaultValue = bool(ie.text)
        else:
            self.showOnDefaultValue = None
        if (ie := element.find("showonunhaulables")) is not None:
            self.showOnUnhaulables = bool(ie.text)
        else:
            self.showOnUnhaulables = None
        if (ie := element.find("showonuntradeables")) is not None:
            self.showOnUntradeables = bool(ie.text)
        else:
            self.showOnUntradeables = None
        if (ie := element.find("neverdisabled")) is not None:
            self.neverDisabled = bool(ie.text)
        else:
            self.neverDisabled = None
        if (ie := element.find("showzerobasevalue")) is not None:
            self.showZeroBaseValue = bool(ie.text)
        else:
            self.showZeroBaseValue = None
        if (ie := element.find("showonslavesonly")) is not None:
            self.showOnSlavesOnly = bool(ie.text)
        else:
            self.showOnSlavesOnly = None
        if (ie := element.find("showonplayermechanoids")) is not None:
            self.showOnPlayerMechanoids = bool(ie.text)
        else:
            self.showOnPlayerMechanoids = None
        if (ie := element.find("showdevelopmentalstagefilter")) is not None:
            self.showDevelopmentalStageFilter = DevelopmentalStage()
            self.showDevelopmentalStageFilter.fromDefElement(tree, ie)
        else:
            self.showDevelopmentalStageFilter = None
        if (ie := element.find("hideinclassicmode")) is not None:
            self.hideInClassicMode = bool(ie.text)
        else:
            self.hideInClassicMode = None
        if (ie := element.find("displaypriorityincategory")) is not None:
            self.displayPriorityInCategory = int(ie.text)
        else:
            self.displayPriorityInCategory = None
        if (ie := element.find("tostringnumbersense")) is not None:
            self.toStringNumberSense = ToStringNumberSense()
            self.toStringNumberSense.fromDefElement(tree, ie)
        else:
            self.toStringNumberSense = None
        if (ie := element.find("tostringstyle")) is not None:
            self.toStringStyle = ToStringStyle()
            self.toStringStyle.fromDefElement(tree, ie)
        else:
            self.toStringStyle = None
        if (ie := element.find("formatstring")) is not None:
            self.formatString = ie.text
        else:
            self.formatString = None
        if (ie := element.find("formatstringunfinalized")) is not None:
            self.formatStringUnfinalized = ie.text
        else:
            self.formatStringUnfinalized = None
        if (ie := element.find("finalizeequippedstatoffset")) is not None:
            self.finalizeEquippedStatOffset = bool(ie.text)
        else:
            self.finalizeEquippedStatOffset = None
        if (ie := element.find("statfactorsexplanationheader")) is not None:
            self.statFactorsExplanationHeader = ie.text
        else:
            self.statFactorsExplanationHeader = None
        if (ie := element.find("defaultbasevalue")) is not None:
            self.defaultBaseValue = float(ie.text)
        else:
            self.defaultBaseValue = None
        if (ie := element.find("noskilloffset")) is not None:
            self.noSkillOffset = float(ie.text)
        else:
            self.noSkillOffset = None
        if (ie := element.find("applyfactorsifnegative")) is not None:
            self.applyFactorsIfNegative = bool(ie.text)
        else:
            self.applyFactorsIfNegative = None
        if (ie := element.find("noskillfactor")) is not None:
            self.noSkillFactor = float(ie.text)
        else:
            self.noSkillFactor = None
        if (ie := element.find("postprocesscurve")) is not None:
            self.postProcessCurve = SimpleCurve()
            self.postProcessCurve.fromDefElement(tree, ie)
        else:
            self.postProcessCurve = None
        if (ie := element.find("minvalue")) is not None:
            self.minValue = float(ie.text)
        else:
            self.minValue = None
        if (ie := element.find("maxvalue")) is not None:
            self.maxValue = float(ie.text)
        else:
            self.maxValue = None
        if (ie := element.find("valueifmissing")) is not None:
            self.valueIfMissing = float(ie.text)
        else:
            self.valueIfMissing = None
        if (ie := element.find("roundvalue")) is not None:
            self.roundValue = bool(ie.text)
        else:
            self.roundValue = None
        if (ie := element.find("roundtofiveover")) is not None:
            self.roundToFiveOver = float(ie.text)
        else:
            self.roundToFiveOver = None
        if (ie := element.find("minifiedthinginherits")) is not None:
            self.minifiedThingInherits = bool(ie.text)
        else:
            self.minifiedThingInherits = None
        if (ie := element.find("supressdisablederror")) is not None:
            self.supressDisabledError = bool(ie.text)
        else:
            self.supressDisabledError = None
        if (ie := element.find("cacheable")) is not None:
            self.cacheable = bool(ie.text)
        else:
            self.cacheable = None
        if (ie := element.find("scenariorandomizable")) is not None:
            self.scenarioRandomizable = bool(ie.text)
        else:
            self.scenarioRandomizable = None
        if (ie := element.find("disableifskilldisabled")) is not None:
            self.disableIfSkillDisabled = SkillDef()
            self.disableIfSkillDisabled.fromDefElement(tree, ie)
        else:
            self.disableIfSkillDisabled = None
        if (ie := element.find("immutable")) is not None:
            self.immutable = bool(ie.text)
        else:
            self.immutable = None

    def toDefInjectElement(
        self, tree: etree._ElementTree, element: etree._Element
    ) -> None:
        super().toDefInjectElement(tree, element)
        etree.SubElement(
            element, f"{self.defName}.labelForFullStatList", {}
        ).text = html.escape(str(self.labelForFullStatList))
        etree.SubElement(
            element, f"{self.defName}.formatString", {}
        ).text = html.escape(str(self.formatString))
        etree.SubElement(
            element, f"{self.defName}.formatStringUnfinalized", {}
        ).text = html.escape(str(self.formatStringUnfinalized))
        etree.SubElement(
            element, f"{self.defName}.statFactorsExplanationHeader", {}
        ).text = html.escape(str(self.statFactorsExplanationHeader))
        return
