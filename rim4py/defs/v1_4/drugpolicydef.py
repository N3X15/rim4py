# @generated by devtools\gen_rim4py_defs.py - DO NOT EDIT BY HAND
# Generated from data\1.4\defs-auto\DrugPolicyDef.yml
from ..exceptions import MissingElementException
import html
from typing import Optional

from lxml import etree

from .def_ import Def

__all__ = ["DrugPolicyDef"]


class DrugPolicyDef(Def):
    TAG: str = "DrugPolicyDef"

    def __init__(self) -> None:
        super().__init__()
        self.allowPleasureDrugs: Optional[bool] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        super().fromDefElement(tree, element)
        if (ie := element.find("allowpleasuredrugs")) is not None:
            self.allowPleasureDrugs = bool(ie.text)
        else:
            self.allowPleasureDrugs = None

    def toDefInjectElement(
        self, tree: etree._ElementTree, element: etree._Element
    ) -> None:
        super().toDefInjectElement(tree, element)
        return
