# @generated by devtools\gen_rim4py_defs.py - DO NOT EDIT BY HAND
# Generated from data\1.4\defs-auto\JobDef.yml
from ..exceptions import MissingElementException
import html
from typing import Optional

from lxml import etree

from .checkjoboverrideondamagemode import CheckJobOverrideOnDamageMode
from .def_ import Def
from .joykinddef import JoyKindDef
from .reservationlayerdef import ReservationLayerDef
from .rot4 import Rot4
from .skilldef import SkillDef
from .taledef import TaleDef

__all__ = ["JobDef"]


class JobDef(Def):
    TAG: str = "JobDef"

    def __init__(self) -> None:
        super().__init__()
        self.driverClass: Optional[str] = None
        self.reportString: Optional[str] = None
        self.playerInterruptible: Optional[bool] = None
        self.forceCompleteBeforeNextJob: Optional[bool] = None
        self.checkOverrideOnDamage: Optional[Def] = None
        self.alwaysShowWeapon: Optional[bool] = None
        self.neverShowWeapon: Optional[bool] = None
        self.suspendable: Optional[bool] = None
        self.casualInterruptible: Optional[bool] = None
        self.allowOpportunisticPrefix: Optional[bool] = None
        self.collideWithPawns: Optional[bool] = None
        self.isIdle: Optional[bool] = None
        self.taleOnCompletion: Optional[Def] = None
        self.neverFleeFromEnemies: Optional[bool] = None
        self.sleepCanInterrupt: Optional[bool] = None
        self.makeTargetPrisoner: Optional[bool] = None
        self.waitAfterArriving: Optional[int] = None
        self.carryThingAfterJob: Optional[bool] = None
        self.dropThingBeforeJob: Optional[bool] = None
        self.joyDuration: Optional[int] = None
        self.joyMaxParticipants: Optional[int] = None
        self.joyGainRate: Optional[float] = None
        self.joySkill: Optional[Def] = None
        self.joyXpPerTick: Optional[float] = None
        self.joyKind: Optional[Def] = None
        self.faceDir: Optional[Def] = None
        self.learningDuration: Optional[int] = None
        self.containerReservationLayer: Optional[Def] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        super().fromDefElement(tree, element)
        if (ie := element.find("driverclass")) is not None:
            self.driverClass = ie.text
        else:
            self.driverClass = None
        if (ie := element.find("reportstring")) is not None:
            self.reportString = ie.text
        else:
            self.reportString = None
        if (ie := element.find("playerinterruptible")) is not None:
            self.playerInterruptible = bool(ie.text)
        else:
            self.playerInterruptible = None
        if (ie := element.find("forcecompletebeforenextjob")) is not None:
            self.forceCompleteBeforeNextJob = bool(ie.text)
        else:
            self.forceCompleteBeforeNextJob = None
        if (ie := element.find("checkoverrideondamage")) is not None:
            self.checkOverrideOnDamage = CheckJobOverrideOnDamageMode()
            self.checkOverrideOnDamage.fromDefElement(tree, ie)
        else:
            self.checkOverrideOnDamage = None
        if (ie := element.find("alwaysshowweapon")) is not None:
            self.alwaysShowWeapon = bool(ie.text)
        else:
            self.alwaysShowWeapon = None
        if (ie := element.find("nevershowweapon")) is not None:
            self.neverShowWeapon = bool(ie.text)
        else:
            self.neverShowWeapon = None
        if (ie := element.find("suspendable")) is not None:
            self.suspendable = bool(ie.text)
        else:
            self.suspendable = None
        if (ie := element.find("casualinterruptible")) is not None:
            self.casualInterruptible = bool(ie.text)
        else:
            self.casualInterruptible = None
        if (ie := element.find("allowopportunisticprefix")) is not None:
            self.allowOpportunisticPrefix = bool(ie.text)
        else:
            self.allowOpportunisticPrefix = None
        if (ie := element.find("collidewithpawns")) is not None:
            self.collideWithPawns = bool(ie.text)
        else:
            self.collideWithPawns = None
        if (ie := element.find("isidle")) is not None:
            self.isIdle = bool(ie.text)
        else:
            self.isIdle = None
        if (ie := element.find("taleoncompletion")) is not None:
            self.taleOnCompletion = TaleDef()
            self.taleOnCompletion.fromDefElement(tree, ie)
        else:
            self.taleOnCompletion = None
        if (ie := element.find("neverfleefromenemies")) is not None:
            self.neverFleeFromEnemies = bool(ie.text)
        else:
            self.neverFleeFromEnemies = None
        if (ie := element.find("sleepcaninterrupt")) is not None:
            self.sleepCanInterrupt = bool(ie.text)
        else:
            self.sleepCanInterrupt = None
        if (ie := element.find("maketargetprisoner")) is not None:
            self.makeTargetPrisoner = bool(ie.text)
        else:
            self.makeTargetPrisoner = None
        if (ie := element.find("waitafterarriving")) is not None:
            self.waitAfterArriving = int(ie.text)
        else:
            self.waitAfterArriving = None
        if (ie := element.find("carrythingafterjob")) is not None:
            self.carryThingAfterJob = bool(ie.text)
        else:
            self.carryThingAfterJob = None
        if (ie := element.find("dropthingbeforejob")) is not None:
            self.dropThingBeforeJob = bool(ie.text)
        else:
            self.dropThingBeforeJob = None
        if (ie := element.find("joyduration")) is not None:
            self.joyDuration = int(ie.text)
        else:
            self.joyDuration = None
        if (ie := element.find("joymaxparticipants")) is not None:
            self.joyMaxParticipants = int(ie.text)
        else:
            self.joyMaxParticipants = None
        if (ie := element.find("joygainrate")) is not None:
            self.joyGainRate = float(ie.text)
        else:
            self.joyGainRate = None
        if (ie := element.find("joyskill")) is not None:
            self.joySkill = SkillDef()
            self.joySkill.fromDefElement(tree, ie)
        else:
            self.joySkill = None
        if (ie := element.find("joyxppertick")) is not None:
            self.joyXpPerTick = float(ie.text)
        else:
            self.joyXpPerTick = None
        if (ie := element.find("joykind")) is not None:
            self.joyKind = JoyKindDef()
            self.joyKind.fromDefElement(tree, ie)
        else:
            self.joyKind = None
        if (ie := element.find("facedir")) is not None:
            self.faceDir = Rot4()
            self.faceDir.fromDefElement(tree, ie)
        else:
            self.faceDir = None
        if (ie := element.find("learningduration")) is not None:
            self.learningDuration = int(ie.text)
        else:
            self.learningDuration = None
        if (ie := element.find("containerreservationlayer")) is not None:
            self.containerReservationLayer = ReservationLayerDef()
            self.containerReservationLayer.fromDefElement(tree, ie)
        else:
            self.containerReservationLayer = None

    def toDefInjectElement(
        self, tree: etree._ElementTree, element: etree._Element
    ) -> None:
        super().toDefInjectElement(tree, element)
        etree.SubElement(
            element, f"{self.defName}.reportString", {}
        ).text = html.escape(str(self.reportString))
        return
