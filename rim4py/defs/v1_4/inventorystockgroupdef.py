# @generated by devtools\gen_rim4py_defs.py - DO NOT EDIT BY HAND
# Generated from data\1.4\defs-auto\InventoryStockGroupDef.yml
from ..exceptions import MissingElementException
import html
from typing import Optional

from lxml import etree

from .def_ import Def
from .thingdef import ThingDef

__all__ = ["InventoryStockGroupDef"]


class InventoryStockGroupDef(Def):
    TAG: str = "InventoryStockGroupDef"

    def __init__(self) -> None:
        super().__init__()
        self.min: Optional[int] = None
        self.max: Optional[int] = None
        self.defaultThingDef: Optional[Def] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        super().fromDefElement(tree, element)
        if (ie := element.find("min")) is not None:
            self.min = int(ie.text)
        else:
            self.min = None
        if (ie := element.find("max")) is not None:
            self.max = int(ie.text)
        else:
            self.max = None
        if (ie := element.find("defaultthingdef")) is not None:
            self.defaultThingDef = ThingDef()
            self.defaultThingDef.fromDefElement(tree, ie)
        else:
            self.defaultThingDef = None

    def toDefInjectElement(
        self, tree: etree._ElementTree, element: etree._Element
    ) -> None:
        super().toDefInjectElement(tree, element)
        return
