# @generated by devtools\gen_rim4py_defs.py - DO NOT EDIT BY HAND
# Generated from data\1.4\defs-auto\FeatureDef.yml
from ..exceptions import MissingElementException
import html
from typing import Optional

from lxml import etree

from .def_ import Def
from .rulepackdef import RulePackDef

__all__ = ["FeatureDef"]


class FeatureDef(Def):
    TAG: str = "FeatureDef"

    def __init__(self) -> None:
        super().__init__()
        self.workerClass: Optional[str] = None
        self.order: Optional[float] = None
        self.minSize: Optional[int] = None
        self.maxSize: Optional[int] = None
        self.canTouchWorldEdge: Optional[bool] = None
        self.nameMaker: Optional[Def] = None
        self.maxPossiblyAllowedSizeToTake: Optional[int] = None
        self.maxPossiblyAllowedSizePctOfMeToTake: Optional[float] = None
        self.maxSpaceBetweenRootGroups: Optional[int] = None
        self.minRootGroupsInCluster: Optional[int] = None
        self.minRootGroupSize: Optional[int] = None
        self.maxRootGroupSize: Optional[int] = None
        self.maxPassageWidth: Optional[int] = None
        self.maxPctOfWholeArea: Optional[float] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        super().fromDefElement(tree, element)
        if (ie := element.find("workerclass")) is not None:
            self.workerClass = ie.text
        else:
            self.workerClass = None
        if (ie := element.find("order")) is not None:
            self.order = float(ie.text)
        else:
            self.order = None
        if (ie := element.find("minsize")) is not None:
            self.minSize = int(ie.text)
        else:
            self.minSize = None
        if (ie := element.find("maxsize")) is not None:
            self.maxSize = int(ie.text)
        else:
            self.maxSize = None
        if (ie := element.find("cantouchworldedge")) is not None:
            self.canTouchWorldEdge = bool(ie.text)
        else:
            self.canTouchWorldEdge = None
        if (ie := element.find("namemaker")) is not None:
            self.nameMaker = RulePackDef()
            self.nameMaker.fromDefElement(tree, ie)
        else:
            self.nameMaker = None
        if (ie := element.find("maxpossiblyallowedsizetotake")) is not None:
            self.maxPossiblyAllowedSizeToTake = int(ie.text)
        else:
            self.maxPossiblyAllowedSizeToTake = None
        if (ie := element.find("maxpossiblyallowedsizepctofmetotake")) is not None:
            self.maxPossiblyAllowedSizePctOfMeToTake = float(ie.text)
        else:
            self.maxPossiblyAllowedSizePctOfMeToTake = None
        if (ie := element.find("maxspacebetweenrootgroups")) is not None:
            self.maxSpaceBetweenRootGroups = int(ie.text)
        else:
            self.maxSpaceBetweenRootGroups = None
        if (ie := element.find("minrootgroupsincluster")) is not None:
            self.minRootGroupsInCluster = int(ie.text)
        else:
            self.minRootGroupsInCluster = None
        if (ie := element.find("minrootgroupsize")) is not None:
            self.minRootGroupSize = int(ie.text)
        else:
            self.minRootGroupSize = None
        if (ie := element.find("maxrootgroupsize")) is not None:
            self.maxRootGroupSize = int(ie.text)
        else:
            self.maxRootGroupSize = None
        if (ie := element.find("maxpassagewidth")) is not None:
            self.maxPassageWidth = int(ie.text)
        else:
            self.maxPassageWidth = None
        if (ie := element.find("maxpctofwholearea")) is not None:
            self.maxPctOfWholeArea = float(ie.text)
        else:
            self.maxPctOfWholeArea = None

    def toDefInjectElement(
        self, tree: etree._ElementTree, element: etree._Element
    ) -> None:
        super().toDefInjectElement(tree, element)
        return
