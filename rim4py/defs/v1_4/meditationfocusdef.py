# @generated by devtools\gen_rim4py_defs.py - DO NOT EDIT BY HAND
# Generated from data\1.4\defs-auto\MeditationFocusDef.yml
from ..exceptions import MissingElementException
import html
from typing import Optional

from lxml import etree

from .def_ import Def

__all__ = ["MeditationFocusDef"]


class MeditationFocusDef(Def):
    TAG: str = "MeditationFocusDef"

    def __init__(self) -> None:
        super().__init__()
        self.requiresRoyalTitle: Optional[bool] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        super().fromDefElement(tree, element)
        if (ie := element.find("requiresroyaltitle")) is not None:
            self.requiresRoyalTitle = bool(ie.text)
        else:
            self.requiresRoyalTitle = None

    def toDefInjectElement(
        self, tree: etree._ElementTree, element: etree._Element
    ) -> None:
        super().toDefInjectElement(tree, element)
        return
