# @generated by devtools\gen_rim4py_defs.py - DO NOT EDIT BY HAND
# Generated from data\1.4\defs-auto\WorldGenStepDef.yml
from ..exceptions import MissingElementException
import html
from typing import Optional

from lxml import etree

from .def_ import Def
from .worldgenstep import WorldGenStep

__all__ = ["WorldGenStepDef"]


class WorldGenStepDef(Def):
    TAG: str = "WorldGenStepDef"

    def __init__(self) -> None:
        super().__init__()
        self.order: Optional[float] = None
        self.worldGenStep: Optional[Def] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        super().fromDefElement(tree, element)
        if (ie := element.find("order")) is not None:
            self.order = float(ie.text)
        else:
            self.order = None
        if (ie := element.find("worldgenstep")) is not None:
            self.worldGenStep = WorldGenStep()
            self.worldGenStep.fromDefElement(tree, ie)
        else:
            self.worldGenStep = None

    def toDefInjectElement(
        self, tree: etree._ElementTree, element: etree._Element
    ) -> None:
        super().toDefInjectElement(tree, element)
        return
