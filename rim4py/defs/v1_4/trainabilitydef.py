# @generated by devtools\gen_rim4py_defs.py - DO NOT EDIT BY HAND
# Generated from data\1.4\defs-auto\TrainabilityDef.yml
from ..exceptions import MissingElementException
import html
from typing import Optional

from lxml import etree

from .def_ import Def

__all__ = ["TrainabilityDef"]


class TrainabilityDef(Def):
    TAG: str = "TrainabilityDef"

    def __init__(self) -> None:
        super().__init__()
        self.intelligenceOrder: Optional[int] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        super().fromDefElement(tree, element)
        if (ie := element.find("intelligenceorder")) is not None:
            self.intelligenceOrder = int(ie.text)
        else:
            self.intelligenceOrder = None

    def toDefInjectElement(
        self, tree: etree._ElementTree, element: etree._Element
    ) -> None:
        super().toDefInjectElement(tree, element)
        return
