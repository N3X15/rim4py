# @generated by devtools\gen_rim4py_defs.py - DO NOT EDIT BY HAND
# Generated from data\1.4\defs-auto\ToolCapacityDef.yml
from ..exceptions import MissingElementException
from lxml import etree

from .def_ import Def

__all__ = ["ToolCapacityDef"]


class ToolCapacityDef(Def):
    TAG: str = "ToolCapacityDef"

    def __init__(self) -> None:
        super().__init__()

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        super().fromDefElement(tree, element)

    def toDefInjectElement(
        self, tree: etree._ElementTree, element: etree._Element
    ) -> None:
        super().toDefInjectElement(tree, element)
        return
