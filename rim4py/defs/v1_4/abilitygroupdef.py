# @generated by devtools\gen_rim4py_defs.py - DO NOT EDIT BY HAND
# Generated from data\1.4\defs-auto\AbilityGroupDef.yml
from ..exceptions import MissingElementException
import html
from typing import Optional

from lxml import etree

from .def_ import Def

__all__ = ["AbilityGroupDef"]


class AbilityGroupDef(Def):
    TAG: str = "AbilityGroupDef"

    def __init__(self) -> None:
        super().__init__()
        self.cooldownTicks: Optional[int] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        super().fromDefElement(tree, element)
        if (ie := element.find("cooldownticks")) is not None:
            self.cooldownTicks = int(ie.text)
        else:
            self.cooldownTicks = None

    def toDefInjectElement(
        self, tree: etree._ElementTree, element: etree._Element
    ) -> None:
        super().toDefInjectElement(tree, element)
        return
