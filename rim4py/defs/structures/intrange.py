from typing import Dict, Optional

from lxml import etree
import re

from ..def_ import Def

REG_INTRANGE = re.compile(r'(-?\d+) *~ *(-?\d+)')
class IntRangeSyntaxError(Exception):
    pass
class IntRange(Def):
    def __init__(self) -> None:
        self.min: int = 0
        self.max: int = 0

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        if (m := REG_INTRANGE.match(element.text)) is not None:
            self.min = int(m[1])
            self.max = int(m[2])
        else:
            raise IntRangeSyntaxError(element.text)

    def toDefInjectElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        pass
