from typing import Dict, Optional

from lxml import etree

from rim4py.defs.v1_3.def_ import Def

class VersionedData(Def):
    def __init__(self) -> None:
        self.descriptions: Dict[str, str] = {}

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        for le in element:
            self.props[le.tag] = le.text

    def toDefInjectElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        pass
