from typing import Optional

from lxml import etree

from ..def_ import Def


class DefHyperlink(Def):
    def __init__(self) -> None:
        self.deftype: Optional[str] = None
        self.defid: Optional[str] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        self.deftype = element.tag
        self.defid = element.text

    def toDefInjectElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        pass
