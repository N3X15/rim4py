from typing import Optional

from lxml import etree

from ..def_ import Def


class WeatherCommonalityRecord(Def):
    def __init__(self) -> None:
        self.stat_type: Optional[str] = None
        self.value: Optional[int] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        self.stat_type = element.tag
        self.value = int(element.text)

    def toDefInjectElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        pass
