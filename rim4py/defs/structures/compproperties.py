from typing import Dict, Optional

from lxml import etree

from ..def_ import Def


class CompProperties(Def):
    def __init__(self) -> None:
        self.cls: Optional[str] = None
        self.props: Dict[str, str] = {}

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        self.cls = element.attrib.get('Class')
        for le in element:
            self.props[le.text] = le.text

    def toDefInjectElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        pass
