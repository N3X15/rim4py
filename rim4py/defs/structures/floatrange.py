from typing import Dict, Optional

from lxml import etree
import re

from ..def_ import Def

REG_FLOATRANGE = re.compile(r'(-?(\d+\.)\d+) *~ *(-?(\d+\.)\d+)')
class FloatRangeSyntaxError(Exception):
    pass
class FloatRange(Def):
    def __init__(self) -> None:
        self.min: float = 0
        self.max: float = 0

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        if (m := REG_FLOATRANGE.match(element.text)) is not None:
            self.min = float(m[1])
            self.max = float(m[2])
        else:
            raise FloatRangeSyntaxError(element.text)

    def toDefInjectElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        pass
