import re
from ..def_ import Def
from lxml import etree
__all__=['Vector3']
REG_VECTOR3_TEXT = re.compile(r'\((\-?(\d+\.)?\d+), *(\-?(\d+\.)?\d+), *(\-?(\d+\.)?\d+)\)')
class Vector3SyntaxError(Exception):
    pass
class Vector3(Def):
    def __init__(self) -> None:
        self.x: float = 0.
        self.y: float = 0.
        self.z: float = 0.
    
    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        if (m := REG_VECTOR3_TEXT.match(element.text)) is not None:
            self.x = float(m[1])
            self.y = float(m[2])
            self.z = float(m[3])
        else:
            raise Vector3SyntaxError(element.text)

    def toDefInjectElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        return