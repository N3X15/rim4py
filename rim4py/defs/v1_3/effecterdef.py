# @generated by devtools\gen_rim4py_defs.py - DO NOT EDIT BY HAND
# Generated from data\1.3\defs-auto\EffecterDef.yml
from ..exceptions import MissingElementException
import html
from typing import Optional

from lxml import etree

from .def_ import Def
from .floatrange import FloatRange

__all__ = ["EffecterDef"]


class EffecterDef(Def):
    TAG: str = "EffecterDef"

    def __init__(self) -> None:
        super().__init__()
        self.positionRadius: Optional[float] = None
        self.offsetTowardsTarget: Optional[Def] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        super().fromDefElement(tree, element)
        if (ie := element.find("positionradius")) is not None:
            self.positionRadius = float(ie.text)
        else:
            self.positionRadius = None
        if (ie := element.find("offsettowardstarget")) is not None:
            self.offsetTowardsTarget = FloatRange()
            self.offsetTowardsTarget.fromDefElement(tree, ie)
        else:
            self.offsetTowardsTarget = None

    def toDefInjectElement(
        self, tree: etree._ElementTree, element: etree._Element
    ) -> None:
        super().toDefInjectElement(tree, element)
        return
