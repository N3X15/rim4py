# @generated by devtools\gen_rim4py_defs.py - DO NOT EDIT BY HAND
# Generated from data\1.3\defs-auto\TraderKindDef.yml
from ..exceptions import MissingElementException
import html
from typing import Optional

from lxml import etree

from .def_ import Def
from .factiondef import FactionDef
from .royaltitlepermitdef import RoyalTitlePermitDef
from .simplecurve import SimpleCurve
from .tradecurrency import TradeCurrency

__all__ = ["TraderKindDef"]


class TraderKindDef(Def):
    TAG: str = "TraderKindDef"

    def __init__(self) -> None:
        super().__init__()
        self.orbital: Optional[bool] = None
        self.requestable: Optional[bool] = None
        self.hideThingsNotWillingToTrade: Optional[bool] = None
        self.commonality: Optional[float] = None
        self.category: Optional[str] = None
        self.tradeCurrency: Optional[Def] = None
        self.commonalityMultFromPopulationIntent: Optional[Def] = None
        self.faction: Optional[Def] = None
        self.permitRequiredForTrading: Optional[Def] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        super().fromDefElement(tree, element)
        if (ie := element.find("orbital")) is not None:
            self.orbital = bool(ie.text)
        else:
            self.orbital = None
        if (ie := element.find("requestable")) is not None:
            self.requestable = bool(ie.text)
        else:
            self.requestable = None
        if (ie := element.find("hidethingsnotwillingtotrade")) is not None:
            self.hideThingsNotWillingToTrade = bool(ie.text)
        else:
            self.hideThingsNotWillingToTrade = None
        if (ie := element.find("commonality")) is not None:
            self.commonality = float(ie.text)
        else:
            self.commonality = None
        if (ie := element.find("category")) is not None:
            self.category = ie.text
        else:
            self.category = None
        if (ie := element.find("tradecurrency")) is not None:
            self.tradeCurrency = TradeCurrency()
            self.tradeCurrency.fromDefElement(tree, ie)
        else:
            self.tradeCurrency = None
        if (ie := element.find("commonalitymultfrompopulationintent")) is not None:
            self.commonalityMultFromPopulationIntent = SimpleCurve()
            self.commonalityMultFromPopulationIntent.fromDefElement(tree, ie)
        else:
            self.commonalityMultFromPopulationIntent = None
        if (ie := element.find("faction")) is not None:
            self.faction = FactionDef()
            self.faction.fromDefElement(tree, ie)
        else:
            self.faction = None
        if (ie := element.find("permitrequiredfortrading")) is not None:
            self.permitRequiredForTrading = RoyalTitlePermitDef()
            self.permitRequiredForTrading.fromDefElement(tree, ie)
        else:
            self.permitRequiredForTrading = None

    def toDefInjectElement(
        self, tree: etree._ElementTree, element: etree._Element
    ) -> None:
        super().toDefInjectElement(tree, element)
        etree.SubElement(element, f"{self.defName}.category", {}).text = html.escape(
            str(self.category)
        )
        return
