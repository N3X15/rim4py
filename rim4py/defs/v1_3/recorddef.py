# @generated by devtools\gen_rim4py_defs.py - DO NOT EDIT BY HAND
# Generated from data\1.3\defs-auto\RecordDef.yml
from ..exceptions import MissingElementException
import html
from typing import Optional

from lxml import etree

from .def_ import Def
from .recordtype import RecordType

__all__ = ["RecordDef"]


class RecordDef(Def):
    TAG: str = "RecordDef"

    def __init__(self) -> None:
        super().__init__()
        self.type: Optional[Def] = None
        self.workerClass: Optional[str] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        super().fromDefElement(tree, element)
        if (ie := element.find("type")) is not None:
            self.type = RecordType()
            self.type.fromDefElement(tree, ie)
        else:
            self.type = None
        if (ie := element.find("workerclass")) is not None:
            self.workerClass = ie.text
        else:
            self.workerClass = None

    def toDefInjectElement(
        self, tree: etree._ElementTree, element: etree._Element
    ) -> None:
        super().toDefInjectElement(tree, element)
        return
