# @generated by devtools\gen_rim4py_defs.py - DO NOT EDIT BY HAND
# Generated from data\1.3\defs-auto\ComplexThreatDef.yml
from ..exceptions import MissingElementException
import html
from typing import Optional

from lxml import etree

from .def_ import Def
from .factiondef import FactionDef
from .signalactionambushtype import SignalActionAmbushType
from .simplecurve import SimpleCurve

__all__ = ["ComplexThreatDef"]


class ComplexThreatDef(Def):
    TAG: str = "ComplexThreatDef"

    def __init__(self) -> None:
        super().__init__()
        self.workerClass: Optional[str] = None
        self.faction: Optional[Def] = None
        self.postSpawnPassiveThreatFactor: Optional[float] = None
        self.minPoints: Optional[int] = None
        self.spawnInOtherRoomChance: Optional[float] = None
        self.allowPassive: Optional[bool] = None
        self.fallbackToRoomEnteredTrigger: Optional[bool] = None
        self.delayChance: Optional[float] = None
        self.threatFactorOverDelayTicksCurve: Optional[Def] = None
        self.signalActionAmbushType: Optional[Def] = None
        self.spawnAroundComplex: Optional[bool] = None
        self.useDropPods: Optional[bool] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        super().fromDefElement(tree, element)
        if (ie := element.find("workerclass")) is not None:
            self.workerClass = ie.text
        else:
            self.workerClass = None
        if (ie := element.find("faction")) is not None:
            self.faction = FactionDef()
            self.faction.fromDefElement(tree, ie)
        else:
            self.faction = None
        if (ie := element.find("postspawnpassivethreatfactor")) is not None:
            self.postSpawnPassiveThreatFactor = float(ie.text)
        else:
            self.postSpawnPassiveThreatFactor = None
        if (ie := element.find("minpoints")) is not None:
            self.minPoints = int(ie.text)
        else:
            self.minPoints = None
        if (ie := element.find("spawninotherroomchance")) is not None:
            self.spawnInOtherRoomChance = float(ie.text)
        else:
            self.spawnInOtherRoomChance = None
        if (ie := element.find("allowpassive")) is not None:
            self.allowPassive = bool(ie.text)
        else:
            self.allowPassive = None
        if (ie := element.find("fallbacktoroomenteredtrigger")) is not None:
            self.fallbackToRoomEnteredTrigger = bool(ie.text)
        else:
            self.fallbackToRoomEnteredTrigger = None
        if (ie := element.find("delaychance")) is not None:
            self.delayChance = float(ie.text)
        else:
            self.delayChance = None
        if (ie := element.find("threatfactoroverdelaytickscurve")) is not None:
            self.threatFactorOverDelayTicksCurve = SimpleCurve()
            self.threatFactorOverDelayTicksCurve.fromDefElement(tree, ie)
        else:
            self.threatFactorOverDelayTicksCurve = None
        if (ie := element.find("signalactionambushtype")) is not None:
            self.signalActionAmbushType = SignalActionAmbushType()
            self.signalActionAmbushType.fromDefElement(tree, ie)
        else:
            self.signalActionAmbushType = None
        if (ie := element.find("spawnaroundcomplex")) is not None:
            self.spawnAroundComplex = bool(ie.text)
        else:
            self.spawnAroundComplex = None
        if (ie := element.find("usedroppods")) is not None:
            self.useDropPods = bool(ie.text)
        else:
            self.useDropPods = None

    def toDefInjectElement(
        self, tree: etree._ElementTree, element: etree._Element
    ) -> None:
        super().toDefInjectElement(tree, element)
        return
