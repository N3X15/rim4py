# @generated by devtools\gen_rim4py_defs.py - DO NOT EDIT BY HAND
# Generated from data\1.3\defs-auto\LogEntryDef.yml
from ..exceptions import MissingElementException
from typing import Optional

from lxml import etree

from .def_ import Def
from .texture2d import Texture2D

__all__ = ["LogEntryDef"]


class LogEntryDef(Def):
    TAG: str = "LogEntryDef"

    def __init__(self) -> None:
        super().__init__()
        self.iconMiss: Optional[str] = None
        self.iconDamaged: Optional[str] = None
        self.iconDamagedFromInstigator: Optional[str] = None
        self.iconMissTex: Optional[Def] = None
        self.iconDamagedTex: Optional[Def] = None
        self.iconDamagedFromInstigatorTex: Optional[Def] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        super().fromDefElement(tree, element)
        if (ie := element.find("iconmiss")) is not None:
            self.iconMiss = ie.text
        else:
            self.iconMiss = None
        if (ie := element.find("icondamaged")) is not None:
            self.iconDamaged = ie.text
        else:
            self.iconDamaged = None
        if (ie := element.find("icondamagedfrominstigator")) is not None:
            self.iconDamagedFromInstigator = ie.text
        else:
            self.iconDamagedFromInstigator = None
        if (ie := element.find("iconmisstex")) is not None:
            self.iconMissTex = Texture2D()
            self.iconMissTex.fromDefElement(tree, ie)
        else:
            self.iconMissTex = None
        if (ie := element.find("icondamagedtex")) is not None:
            self.iconDamagedTex = Texture2D()
            self.iconDamagedTex.fromDefElement(tree, ie)
        else:
            self.iconDamagedTex = None
        if (ie := element.find("icondamagedfrominstigatortex")) is not None:
            self.iconDamagedFromInstigatorTex = Texture2D()
            self.iconDamagedFromInstigatorTex.fromDefElement(tree, ie)
        else:
            self.iconDamagedFromInstigatorTex = None

    def toDefInjectElement(
        self, tree: etree._ElementTree, element: etree._Element
    ) -> None:
        super().toDefInjectElement(tree, element)
        return
