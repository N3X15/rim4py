from __future__ import annotations

from typing import Dict, Type

from .abilitycategorydef import AbilityCategoryDef
from .abilitydef import AbilityDef
from .abilitygroupdef import AbilityGroupDef
from .apparellayerdef import ApparelLayerDef
from .billrepeatmodedef import BillRepeatModeDef
from .billstoremodedef import BillStoreModeDef
from .biomedef import BiomeDef
from .bodydef import BodyDef
from .bodypartdef import BodyPartDef
from .bodypartgroupdef import BodyPartGroupDef
from .bodyparttagdef import BodyPartTagDef
from .bodytypedef import BodyTypeDef
from .buildabledef import BuildableDef
from .chemicaldef import ChemicalDef
from .clamordef import ClamorDef
from .complexdef import ComplexDef
from .complexroomdef import ComplexRoomDef
from .complexthreatdef import ComplexThreatDef
from .conceptdef import ConceptDef
from .culturedef import CultureDef
from .damagearmorcategorydef import DamageArmorCategoryDef
from .damagedef import DamageDef
from .def_ import Def
from .designationcategorydef import DesignationCategoryDef
from .designationdef import DesignationDef
from .designatordropdowngroupdef import DesignatorDropdownGroupDef
from .difficultydef import DifficultyDef
from .drugpolicydef import DrugPolicyDef
from .dutydef import DutyDef
from .effecterdef import EffecterDef
from .expansiondef import ExpansionDef
from .expectationdef import ExpectationDef
from .factiondef import FactionDef
from .featuredef import FeatureDef
from .fleckdef import FleckDef
from .fleshtypedef import FleshTypeDef
from .gameconditiondef import GameConditionDef
from .gatheringdef import GatheringDef
from .gauranlentreemodedef import GauranlenTreeModeDef
from .genstepdef import GenStepDef
from .goodwillsituationdef import GoodwillSituationDef
from .hediffdef import HediffDef
from .hediffgiversetdef import HediffGiverSetDef
from .hibernatablestatedef import HibernatableStateDef
from .historyautorecorderdef import HistoryAutoRecorderDef
from .historyautorecordergroupdef import HistoryAutoRecorderGroupDef
from .historyeventdef import HistoryEventDef
from .ideofoundationdef import IdeoFoundationDef
from .ideopresetcategorydef import IdeoPresetCategoryDef
from .ideopresetdef import IdeoPresetDef
from .ideostorypatterndef import IdeoStoryPatternDef
from .ideosymbolpartdef import IdeoSymbolPartDef
from .impactsoundtypedef import ImpactSoundTypeDef
from .implementownertypedef import ImplementOwnerTypeDef
from .incidentcategorydef import IncidentCategoryDef
from .incidentdef import IncidentDef
from .incidenttargettagdef import IncidentTargetTagDef
from .inspirationdef import InspirationDef
from .instructiondef import InstructionDef
from .interactiondef import InteractionDef
from .inventorystockgroupdef import InventoryStockGroupDef
from .issuedef import IssueDef
from .jobdef import JobDef
from .joygiverdef import JoyGiverDef
from .joykinddef import JoyKindDef
from .keybindingcategorydef import KeyBindingCategoryDef
from .keybindingdef import KeyBindingDef
from .letterdef import LetterDef
from .lifestagedef import LifeStageDef
from .logentrydef import LogEntryDef
from .mainbuttondef import MainButtonDef
from .maneuverdef import ManeuverDef
from .mapgeneratordef import MapGeneratorDef
from .meditationfocusdef import MeditationFocusDef
from .memedef import MemeDef
from .memegroupdef import MemeGroupDef
from .mentalbreakdef import MentalBreakDef
from .mentalstatedef import MentalStateDef
from .messagetypedef import MessageTypeDef
from .modmetadatainternal import ModMetaDataInternal
from .needdef import NeedDef
from .orderedtakegroupdef import OrderedTakeGroupDef
from .pawncapacitydef import PawnCapacityDef
from .pawncolumndef import PawnColumnDef
from .pawngroupkinddef import PawnGroupKindDef
from .pawnkinddef import PawnKindDef
from .pawnrelationdef import PawnRelationDef
from .pawnsarrivalmodedef import PawnsArrivalModeDef
from .pawntabledef import PawnTableDef
from .placedef import PlaceDef
from .preceptdef import PreceptDef
from .prisonerinteractionmodedef import PrisonerInteractionModeDef
from .questscriptdef import QuestScriptDef
from .raidstrategydef import RaidStrategyDef
from .recipedef import RecipeDef
from .recorddef import RecordDef
from .researchprojectdef import ResearchProjectDef
from .researchprojecttagdef import ResearchProjectTagDef
from .researchtabdef import ResearchTabDef
from .reservationlayerdef import ReservationLayerDef
from .ritualattachableoutcomeeffectdef import RitualAttachableOutcomeEffectDef
from .ritualbehaviordef import RitualBehaviorDef
from .ritualobligationtargetfilterdef import RitualObligationTargetFilterDef
from .ritualoutcomeeffectdef import RitualOutcomeEffectDef
from .ritualpatterndef import RitualPatternDef
from .ritualtargetfilterdef import RitualTargetFilterDef
from .ritualvisualeffectdef import RitualVisualEffectDef
from .riverdef import RiverDef
from .roaddef import RoadDef
from .roadpathingdef import RoadPathingDef
from .roadworldlayerdef import RoadWorldLayerDef
from .roofdef import RoofDef
from .roomroledef import RoomRoleDef
from .roomstatdef import RoomStatDef
from .royaltitledef import RoyalTitleDef
from .royaltitlepermitdef import RoyalTitlePermitDef
from .ruledef import RuleDef
from .rulepackdef import RulePackDef
from .scatterabledef import ScatterableDef
from .scenariodef import ScenarioDef
from .scenpartdef import ScenPartDef
from .shadertypedef import ShaderTypeDef
from .shipjobdef import ShipJobDef
from .sitepartdef import SitePartDef
from .sketchresolverdef import SketchResolverDef
from .skilldef import SkillDef
from .slaveinteractionmodedef import SlaveInteractionModeDef
from .songdef import SongDef
from .sounddef import SoundDef
from .specialthingfilterdef import SpecialThingFilterDef
from .statcategorydef import StatCategoryDef
from .statdef import StatDef
from .storytellerdef import StorytellerDef
from .stuffappearancedef import StuffAppearanceDef
from .stuffcategorydef import StuffCategoryDef
from .stylecategorydef import StyleCategoryDef
from .styleitemcategorydef import StyleItemCategoryDef
from .styleitemdef import StyleItemDef
from .subcameradef import SubcameraDef
from .taledef import TaleDef
from .terrainaffordancedef import TerrainAffordanceDef
from .thingcategorydef import ThingCategoryDef
from .thingsetmakerdef import ThingSetMakerDef
from .thingstyledef import ThingStyleDef
from .thinktreedef import ThinkTreeDef
from .thoughtdef import ThoughtDef
from .timeassignmentdef import TimeAssignmentDef
from .tipsetdef import TipSetDef
from .toolcapacitydef import ToolCapacityDef
from .traderkinddef import TraderKindDef
from .trainabilitydef import TrainabilityDef
from .trainabledef import TrainableDef
from .traitdef import TraitDef
from .transferablesorterdef import TransferableSorterDef
from .transportshipdef import TransportShipDef
from .weaponclassdef import WeaponClassDef
from .weaponclasspairdef import WeaponClassPairDef
from .weapontraitdef import WeaponTraitDef
from .weatherdef import WeatherDef
from .workgiverdef import WorkGiverDef
from .workgiverequivalencegroupdef import WorkGiverEquivalenceGroupDef
from .worktypedef import WorkTypeDef
from .worldgenstepdef import WorldGenStepDef
from .worldobjectdef import WorldObjectDef

DEF_TYPES: Dict[str, Type[Def]] = {
    "abilitycategorydef": AbilityCategoryDef,
    "abilitydef": AbilityDef,
    "abilitygroupdef": AbilityGroupDef,
    "apparellayerdef": ApparelLayerDef,
    "billrepeatmodedef": BillRepeatModeDef,
    "billstoremodedef": BillStoreModeDef,
    "biomedef": BiomeDef,
    "bodydef": BodyDef,
    "bodypartdef": BodyPartDef,
    "bodypartgroupdef": BodyPartGroupDef,
    "bodyparttagdef": BodyPartTagDef,
    "bodytypedef": BodyTypeDef,
    "buildabledef": BuildableDef,
    "chemicaldef": ChemicalDef,
    "clamordef": ClamorDef,
    "complexdef": ComplexDef,
    "complexroomdef": ComplexRoomDef,
    "complexthreatdef": ComplexThreatDef,
    "conceptdef": ConceptDef,
    "culturedef": CultureDef,
    "damagearmorcategorydef": DamageArmorCategoryDef,
    "damagedef": DamageDef,
    "def": Def,
    "designationcategorydef": DesignationCategoryDef,
    "designationdef": DesignationDef,
    "designatordropdowngroupdef": DesignatorDropdownGroupDef,
    "difficultydef": DifficultyDef,
    "drugpolicydef": DrugPolicyDef,
    "dutydef": DutyDef,
    "effecterdef": EffecterDef,
    "expansiondef": ExpansionDef,
    "expectationdef": ExpectationDef,
    "factiondef": FactionDef,
    "featuredef": FeatureDef,
    "fleckdef": FleckDef,
    "fleshtypedef": FleshTypeDef,
    "gameconditiondef": GameConditionDef,
    "gatheringdef": GatheringDef,
    "gauranlentreemodedef": GauranlenTreeModeDef,
    "genstepdef": GenStepDef,
    "goodwillsituationdef": GoodwillSituationDef,
    "hediffdef": HediffDef,
    "hediffgiversetdef": HediffGiverSetDef,
    "hibernatablestatedef": HibernatableStateDef,
    "historyautorecorderdef": HistoryAutoRecorderDef,
    "historyautorecordergroupdef": HistoryAutoRecorderGroupDef,
    "historyeventdef": HistoryEventDef,
    "ideofoundationdef": IdeoFoundationDef,
    "ideopresetcategorydef": IdeoPresetCategoryDef,
    "ideopresetdef": IdeoPresetDef,
    "ideostorypatterndef": IdeoStoryPatternDef,
    "ideosymbolpartdef": IdeoSymbolPartDef,
    "impactsoundtypedef": ImpactSoundTypeDef,
    "implementownertypedef": ImplementOwnerTypeDef,
    "incidentcategorydef": IncidentCategoryDef,
    "incidentdef": IncidentDef,
    "incidenttargettagdef": IncidentTargetTagDef,
    "inspirationdef": InspirationDef,
    "instructiondef": InstructionDef,
    "interactiondef": InteractionDef,
    "inventorystockgroupdef": InventoryStockGroupDef,
    "issuedef": IssueDef,
    "jobdef": JobDef,
    "joygiverdef": JoyGiverDef,
    "joykinddef": JoyKindDef,
    "keybindingcategorydef": KeyBindingCategoryDef,
    "keybindingdef": KeyBindingDef,
    "letterdef": LetterDef,
    "lifestagedef": LifeStageDef,
    "logentrydef": LogEntryDef,
    "mainbuttondef": MainButtonDef,
    "maneuverdef": ManeuverDef,
    "mapgeneratordef": MapGeneratorDef,
    "meditationfocusdef": MeditationFocusDef,
    "memedef": MemeDef,
    "memegroupdef": MemeGroupDef,
    "mentalbreakdef": MentalBreakDef,
    "mentalstatedef": MentalStateDef,
    "messagetypedef": MessageTypeDef,
    "modmetadatainternal": ModMetaDataInternal,
    "needdef": NeedDef,
    "orderedtakegroupdef": OrderedTakeGroupDef,
    "pawncapacitydef": PawnCapacityDef,
    "pawncolumndef": PawnColumnDef,
    "pawngroupkinddef": PawnGroupKindDef,
    "pawnkinddef": PawnKindDef,
    "pawnrelationdef": PawnRelationDef,
    "pawnsarrivalmodedef": PawnsArrivalModeDef,
    "pawntabledef": PawnTableDef,
    "placedef": PlaceDef,
    "preceptdef": PreceptDef,
    "prisonerinteractionmodedef": PrisonerInteractionModeDef,
    "questscriptdef": QuestScriptDef,
    "raidstrategydef": RaidStrategyDef,
    "recipedef": RecipeDef,
    "recorddef": RecordDef,
    "researchprojectdef": ResearchProjectDef,
    "researchprojecttagdef": ResearchProjectTagDef,
    "researchtabdef": ResearchTabDef,
    "reservationlayerdef": ReservationLayerDef,
    "ritualattachableoutcomeeffectdef": RitualAttachableOutcomeEffectDef,
    "ritualbehaviordef": RitualBehaviorDef,
    "ritualobligationtargetfilterdef": RitualObligationTargetFilterDef,
    "ritualoutcomeeffectdef": RitualOutcomeEffectDef,
    "ritualpatterndef": RitualPatternDef,
    "ritualtargetfilterdef": RitualTargetFilterDef,
    "ritualvisualeffectdef": RitualVisualEffectDef,
    "riverdef": RiverDef,
    "roaddef": RoadDef,
    "roadpathingdef": RoadPathingDef,
    "roadworldlayerdef": RoadWorldLayerDef,
    "roofdef": RoofDef,
    "roomroledef": RoomRoleDef,
    "roomstatdef": RoomStatDef,
    "royaltitledef": RoyalTitleDef,
    "royaltitlepermitdef": RoyalTitlePermitDef,
    "ruledef": RuleDef,
    "rulepackdef": RulePackDef,
    "scatterabledef": ScatterableDef,
    "scenariodef": ScenarioDef,
    "scenpartdef": ScenPartDef,
    "shadertypedef": ShaderTypeDef,
    "shipjobdef": ShipJobDef,
    "sitepartdef": SitePartDef,
    "sketchresolverdef": SketchResolverDef,
    "skilldef": SkillDef,
    "slaveinteractionmodedef": SlaveInteractionModeDef,
    "songdef": SongDef,
    "sounddef": SoundDef,
    "specialthingfilterdef": SpecialThingFilterDef,
    "statcategorydef": StatCategoryDef,
    "statdef": StatDef,
    "storytellerdef": StorytellerDef,
    "stuffappearancedef": StuffAppearanceDef,
    "stuffcategorydef": StuffCategoryDef,
    "stylecategorydef": StyleCategoryDef,
    "styleitemcategorydef": StyleItemCategoryDef,
    "styleitemdef": StyleItemDef,
    "subcameradef": SubcameraDef,
    "taledef": TaleDef,
    "terrainaffordancedef": TerrainAffordanceDef,
    "thingcategorydef": ThingCategoryDef,
    "thingsetmakerdef": ThingSetMakerDef,
    "thingstyledef": ThingStyleDef,
    "thinktreedef": ThinkTreeDef,
    "thoughtdef": ThoughtDef,
    "timeassignmentdef": TimeAssignmentDef,
    "tipsetdef": TipSetDef,
    "toolcapacitydef": ToolCapacityDef,
    "traderkinddef": TraderKindDef,
    "trainabilitydef": TrainabilityDef,
    "trainabledef": TrainableDef,
    "traitdef": TraitDef,
    "transferablesorterdef": TransferableSorterDef,
    "transportshipdef": TransportShipDef,
    "weaponclassdef": WeaponClassDef,
    "weaponclasspairdef": WeaponClassPairDef,
    "weapontraitdef": WeaponTraitDef,
    "weatherdef": WeatherDef,
    "workgiverdef": WorkGiverDef,
    "workgiverequivalencegroupdef": WorkGiverEquivalenceGroupDef,
    "worktypedef": WorkTypeDef,
    "worldgenstepdef": WorldGenStepDef,
    "worldobjectdef": WorldObjectDef,
}
