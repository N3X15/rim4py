# @generated by devtools\gen_rim4py_defs.py - DO NOT EDIT BY HAND
# Generated from data\1.3\defs-auto\NeedDef.yml
from ..exceptions import MissingElementException
import html
from typing import Optional

from lxml import etree

from .def_ import Def
from .intelligence import Intelligence

__all__ = ["NeedDef"]


class NeedDef(Def):
    TAG: str = "NeedDef"

    def __init__(self) -> None:
        super().__init__()
        self.needClass: Optional[str] = None
        self.minIntelligence: Optional[Def] = None
        self.colonistAndPrisonersOnly: Optional[bool] = None
        self.colonistsOnly: Optional[bool] = None
        self.slavesOnly: Optional[bool] = None
        self.onlyIfCausedByHediff: Optional[bool] = None
        self.onlyIfCausedByTrait: Optional[bool] = None
        self.neverOnPrisoner: Optional[bool] = None
        self.neverOnSlave: Optional[bool] = None
        self.showOnNeedList: Optional[bool] = None
        self.baseLevel: Optional[float] = None
        self.major: Optional[bool] = None
        self.listPriority: Optional[int] = None
        self.tutorHighlightTag: Optional[str] = None
        self.showForCaravanMembers: Optional[bool] = None
        self.scaleBar: Optional[bool] = None
        self.fallPerDay: Optional[float] = None
        self.seekerRisePerHour: Optional[float] = None
        self.seekerFallPerHour: Optional[float] = None
        self.freezeWhileSleeping: Optional[bool] = None
        self.freezeInMentalState: Optional[bool] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        super().fromDefElement(tree, element)
        if (ie := element.find("needclass")) is not None:
            self.needClass = ie.text
        else:
            self.needClass = None
        if (ie := element.find("minintelligence")) is not None:
            self.minIntelligence = Intelligence()
            self.minIntelligence.fromDefElement(tree, ie)
        else:
            self.minIntelligence = None
        if (ie := element.find("colonistandprisonersonly")) is not None:
            self.colonistAndPrisonersOnly = bool(ie.text)
        else:
            self.colonistAndPrisonersOnly = None
        if (ie := element.find("colonistsonly")) is not None:
            self.colonistsOnly = bool(ie.text)
        else:
            self.colonistsOnly = None
        if (ie := element.find("slavesonly")) is not None:
            self.slavesOnly = bool(ie.text)
        else:
            self.slavesOnly = None
        if (ie := element.find("onlyifcausedbyhediff")) is not None:
            self.onlyIfCausedByHediff = bool(ie.text)
        else:
            self.onlyIfCausedByHediff = None
        if (ie := element.find("onlyifcausedbytrait")) is not None:
            self.onlyIfCausedByTrait = bool(ie.text)
        else:
            self.onlyIfCausedByTrait = None
        if (ie := element.find("neveronprisoner")) is not None:
            self.neverOnPrisoner = bool(ie.text)
        else:
            self.neverOnPrisoner = None
        if (ie := element.find("neveronslave")) is not None:
            self.neverOnSlave = bool(ie.text)
        else:
            self.neverOnSlave = None
        if (ie := element.find("showonneedlist")) is not None:
            self.showOnNeedList = bool(ie.text)
        else:
            self.showOnNeedList = None
        if (ie := element.find("baselevel")) is not None:
            self.baseLevel = float(ie.text)
        else:
            self.baseLevel = None
        if (ie := element.find("major")) is not None:
            self.major = bool(ie.text)
        else:
            self.major = None
        if (ie := element.find("listpriority")) is not None:
            self.listPriority = int(ie.text)
        else:
            self.listPriority = None
        if (ie := element.find("tutorhighlighttag")) is not None:
            self.tutorHighlightTag = ie.text
        else:
            self.tutorHighlightTag = None
        if (ie := element.find("showforcaravanmembers")) is not None:
            self.showForCaravanMembers = bool(ie.text)
        else:
            self.showForCaravanMembers = None
        if (ie := element.find("scalebar")) is not None:
            self.scaleBar = bool(ie.text)
        else:
            self.scaleBar = None
        if (ie := element.find("fallperday")) is not None:
            self.fallPerDay = float(ie.text)
        else:
            self.fallPerDay = None
        if (ie := element.find("seekerriseperhour")) is not None:
            self.seekerRisePerHour = float(ie.text)
        else:
            self.seekerRisePerHour = None
        if (ie := element.find("seekerfallperhour")) is not None:
            self.seekerFallPerHour = float(ie.text)
        else:
            self.seekerFallPerHour = None
        if (ie := element.find("freezewhilesleeping")) is not None:
            self.freezeWhileSleeping = bool(ie.text)
        else:
            self.freezeWhileSleeping = None
        if (ie := element.find("freezeinmentalstate")) is not None:
            self.freezeInMentalState = bool(ie.text)
        else:
            self.freezeInMentalState = None

    def toDefInjectElement(
        self, tree: etree._ElementTree, element: etree._Element
    ) -> None:
        super().toDefInjectElement(tree, element)
        return
