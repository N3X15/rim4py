# @generated by devtools\gen_rim4py_defs.py - DO NOT EDIT BY HAND
# Generated from data\1.3\defs-auto\DamageArmorCategoryDef.yml
from ..exceptions import MissingElementException
import html
from typing import Optional

from lxml import etree

from .def_ import Def
from .statdef import StatDef

__all__ = ["DamageArmorCategoryDef"]


class DamageArmorCategoryDef(Def):
    TAG: str = "DamageArmorCategoryDef"

    def __init__(self) -> None:
        super().__init__()
        self.multStat: Optional[Def] = None
        self.armorRatingStat: Optional[Def] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        super().fromDefElement(tree, element)
        if (ie := element.find("multstat")) is not None:
            self.multStat = StatDef()
            self.multStat.fromDefElement(tree, ie)
        else:
            self.multStat = None
        if (ie := element.find("armorratingstat")) is not None:
            self.armorRatingStat = StatDef()
            self.armorRatingStat.fromDefElement(tree, ie)
        else:
            self.armorRatingStat = None

    def toDefInjectElement(
        self, tree: etree._ElementTree, element: etree._Element
    ) -> None:
        super().toDefInjectElement(tree, element)
        return
