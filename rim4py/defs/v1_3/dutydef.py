# @generated by devtools\gen_rim4py_defs.py - DO NOT EDIT BY HAND
# Generated from data\1.3\defs-auto\DutyDef.yml
from ..exceptions import MissingElementException
import html
from typing import Optional

from lxml import etree

from .def_ import Def
from .randomsocialmode import RandomSocialMode
from .thinknode import ThinkNode
from .thinktreedutyhook import ThinkTreeDutyHook

__all__ = ["DutyDef"]


class DutyDef(Def):
    TAG: str = "DutyDef"

    def __init__(self) -> None:
        super().__init__()
        self.thinkNode: Optional[Def] = None
        self.constantThinkNode: Optional[Def] = None
        self.alwaysShowWeapon: Optional[bool] = None
        self.hook: Optional[Def] = None
        self.socialModeMax: Optional[Def] = None
        self.threatDisabled: Optional[bool] = None
        self.ritualSpectateTarget: Optional[bool] = None

    def fromDefElement(self, tree: etree._ElementTree, element: etree._Element) -> None:
        super().fromDefElement(tree, element)
        if (ie := element.find("thinknode")) is not None:
            self.thinkNode = ThinkNode()
            self.thinkNode.fromDefElement(tree, ie)
        else:
            self.thinkNode = None
        if (ie := element.find("constantthinknode")) is not None:
            self.constantThinkNode = ThinkNode()
            self.constantThinkNode.fromDefElement(tree, ie)
        else:
            self.constantThinkNode = None
        if (ie := element.find("alwaysshowweapon")) is not None:
            self.alwaysShowWeapon = bool(ie.text)
        else:
            self.alwaysShowWeapon = None
        if (ie := element.find("hook")) is not None:
            self.hook = ThinkTreeDutyHook()
            self.hook.fromDefElement(tree, ie)
        else:
            self.hook = None
        if (ie := element.find("socialmodemax")) is not None:
            self.socialModeMax = RandomSocialMode()
            self.socialModeMax.fromDefElement(tree, ie)
        else:
            self.socialModeMax = None
        if (ie := element.find("threatdisabled")) is not None:
            self.threatDisabled = bool(ie.text)
        else:
            self.threatDisabled = None
        if (ie := element.find("ritualspectatetarget")) is not None:
            self.ritualSpectateTarget = bool(ie.text)
        else:
            self.ritualSpectateTarget = None

    def toDefInjectElement(
        self, tree: etree._ElementTree, element: etree._Element
    ) -> None:
        super().toDefInjectElement(tree, element)
        return
