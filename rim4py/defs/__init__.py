from __future__ import annotations

from typing import Dict, Type

from .abilitycategorydef import AbilityCategoryDef
from .abilitydef import AbilityDef
from .abilitygroupdef import AbilityGroupDef
from .apparellayerdef import ApparelLayerDef
from .billrepeatmodedef import BillRepeatModeDef
from .billstoremodedef import BillStoreModeDef
from .biomedef import BiomeDef
from .bodydef import BodyDef
from .bodypartdef import BodyPartDef
from .bodypartgroupdef import BodyPartGroupDef
from .bodypartrecord import BodyPartRecord
from .bodyparttagdef import BodyPartTagDef
from .bodytypedef import BodyTypeDef
from .buildabledef import BuildableDef
from .chemicaldef import ChemicalDef
from .clamordef import ClamorDef
from .complexdef import ComplexDef
from .complexroomdef import ComplexRoomDef
from .complexthreatdef import ComplexThreatDef
from .conceptdef import ConceptDef
from .culturedef import CultureDef
from .damagearmorcategorydef import DamageArmorCategoryDef
from .damagedef import DamageDef
from .def_ import Def
from .designationcategorydef import DesignationCategoryDef
from .designationdef import DesignationDef
from .designatordropdowngroupdef import DesignatorDropdownGroupDef
from .difficultydef import DifficultyDef
from .drugpolicydef import DrugPolicyDef
from .dutydef import DutyDef
from .effecterdef import EffecterDef
from .expansiondef import ExpansionDef
from .expectationdef import ExpectationDef
from .factiondef import FactionDef
from .featuredef import FeatureDef
from .fleckdef import FleckDef
from .fleshtypedef import FleshTypeDef
from .gameconditiondef import GameConditionDef
from .gatheringdef import GatheringDef
from .gauranlentreemodedef import GauranlenTreeModeDef
from .genstepdef import GenStepDef
from .goodwillsituationdef import GoodwillSituationDef
from .hediffdef import HediffDef
from .hediffgiversetdef import HediffGiverSetDef
from .hibernatablestatedef import HibernatableStateDef
from .historyautorecorderdef import HistoryAutoRecorderDef
from .historyautorecordergroupdef import HistoryAutoRecorderGroupDef
from .historyeventdef import HistoryEventDef
from .ideofoundationdef import IdeoFoundationDef
from .ideopresetcategorydef import IdeoPresetCategoryDef
from .ideopresetdef import IdeoPresetDef
from .ideostorypatterndef import IdeoStoryPatternDef
from .ideosymbolpartdef import IdeoSymbolPartDef
from .impactsoundtypedef import ImpactSoundTypeDef
from .implementownertypedef import ImplementOwnerTypeDef
from .incidentcategorydef import IncidentCategoryDef
from .incidentdef import IncidentDef
from .incidenttargettagdef import IncidentTargetTagDef
from .inspirationdef import InspirationDef
from .instructiondef import InstructionDef
from .interactiondef import InteractionDef
from .inventorystockgroupdef import InventoryStockGroupDef
from .issuedef import IssueDef
from .jobdef import JobDef
from .joygiverdef import JoyGiverDef
from .joykinddef import JoyKindDef
from .keybindingcategorydef import KeyBindingCategoryDef
from .keybindingdef import KeyBindingDef
from .letterdef import LetterDef
from .lifestagedef import LifeStageDef
from .logentrydef import LogEntryDef
from .mainbuttondef import MainButtonDef
from .maneuverdef import ManeuverDef
from .mapgeneratordef import MapGeneratorDef
from .meditationfocusdef import MeditationFocusDef
from .memedef import MemeDef
from .memegroupdef import MemeGroupDef
from .mentalbreakdef import MentalBreakDef
from .mentalstatedef import MentalStateDef
from .messagetypedef import MessageTypeDef
from .needdef import NeedDef
from .orderedtakegroupdef import OrderedTakeGroupDef
from .pawncapacitydef import PawnCapacityDef
from .pawncolumndef import PawnColumnDef
from .pawngroupkinddef import PawnGroupKindDef
from .pawnkinddef import PawnKindDef
from .pawnrelationdef import PawnRelationDef
from .pawnsarrivalmodedef import PawnsArrivalModeDef
from .pawntabledef import PawnTableDef
from .placedef import PlaceDef
from .preceptdef import PreceptDef
from .prisonerinteractionmodedef import PrisonerInteractionModeDef
from .questscriptdef import QuestScriptDef
from .raidstrategydef import RaidStrategyDef
from .recipedef import RecipeDef
from .recorddef import RecordDef
from .researchprojectdef import ResearchProjectDef
from .researchprojecttagdef import ResearchProjectTagDef
from .researchtabdef import ResearchTabDef
from .reservationlayerdef import ReservationLayerDef
from .ritualattachableoutcomeeffectdef import RitualAttachableOutcomeEffectDef
from .ritualbehaviordef import RitualBehaviorDef
from .ritualobligationtargetfilterdef import RitualObligationTargetFilterDef
from .ritualoutcomeeffectdef import RitualOutcomeEffectDef
from .ritualpatterndef import RitualPatternDef
from .ritualtargetfilterdef import RitualTargetFilterDef
from .ritualvisualeffectdef import RitualVisualEffectDef
from .riverdef import RiverDef
from .roaddef import RoadDef
from .roadpathingdef import RoadPathingDef
from .roadworldlayerdef import RoadWorldLayerDef
from .roofdef import RoofDef
from .roomroledef import RoomRoleDef
from .roomstatdef import RoomStatDef
from .royaltitledef import RoyalTitleDef
from .royaltitlepermitdef import RoyalTitlePermitDef
from .ruledef import RuleDef
from .rulepackdef import RulePackDef
from .scatterabledef import ScatterableDef
from .scenariodef import ScenarioDef
from .scenpartdef import ScenPartDef
from .shadertypedef import ShaderTypeDef
from .shipjobdef import ShipJobDef
from .sitepartdef import SitePartDef
from .sketchresolverdef import SketchResolverDef
from .skilldef import SkillDef
from .slaveinteractionmodedef import SlaveInteractionModeDef
from .songdef import SongDef
from .sounddef import SoundDef
from .specialthingfilterdef import SpecialThingFilterDef
from .statcategorydef import StatCategoryDef
from .statdef import StatDef
from .storytellerdef import StorytellerDef
from .stuffappearancedef import StuffAppearanceDef
from .stuffcategorydef import StuffCategoryDef
from .stylecategorydef import StyleCategoryDef
from .styleitemcategorydef import StyleItemCategoryDef
from .styleitemdef import StyleItemDef
from .subcameradef import SubcameraDef
from .taledef import TaleDef
from .terrainaffordancedef import TerrainAffordanceDef
from .terrainpatchmaker import TerrainPatchMaker
from .terrainthreshold import TerrainThreshold
from .thingcategorydef import ThingCategoryDef
from .thingsetmakerdef import ThingSetMakerDef
from .thingstyledef import ThingStyleDef
from .thinktreedef import ThinkTreeDef
from .thoughtdef import ThoughtDef
from .timeassignmentdef import TimeAssignmentDef
from .tipsetdef import TipSetDef
from .toolcapacitydef import ToolCapacityDef
from .traderkinddef import TraderKindDef
from .trainabilitydef import TrainabilityDef
from .trainabledef import TrainableDef
from .traitdef import TraitDef
from .transferablesorterdef import TransferableSorterDef
from .transportshipdef import TransportShipDef
from .verbproperties import VerbProperties
from .weaponclassdef import WeaponClassDef
from .weaponclasspairdef import WeaponClassPairDef
from .weapontraitdef import WeaponTraitDef
from .weatherdef import WeatherDef
from .workgiverdef import WorkGiverDef
from .workgiverequivalencegroupdef import WorkGiverEquivalenceGroupDef
from .worktypedef import WorkTypeDef
from .worldgenstepdef import WorldGenStepDef
from .worldobjectdef import WorldObjectDef

DEF_TYPES: Dict[str, Type[Def]] = {
    "AbilityCategoryDef": AbilityCategoryDef,
    "AbilityDef": AbilityDef,
    "AbilityGroupDef": AbilityGroupDef,
    "ApparelLayerDef": ApparelLayerDef,
    "BillRepeatModeDef": BillRepeatModeDef,
    "BillStoreModeDef": BillStoreModeDef,
    "BiomeDef": BiomeDef,
    "BodyDef": BodyDef,
    "BodyPartDef": BodyPartDef,
    "BodyPartGroupDef": BodyPartGroupDef,
    "BodyPartRecord": BodyPartRecord,
    "BodyPartTagDef": BodyPartTagDef,
    "BodyTypeDef": BodyTypeDef,
    "BuildableDef": BuildableDef,
    "ChemicalDef": ChemicalDef,
    "ClamorDef": ClamorDef,
    "ComplexDef": ComplexDef,
    "ComplexRoomDef": ComplexRoomDef,
    "ComplexThreatDef": ComplexThreatDef,
    "ConceptDef": ConceptDef,
    "CultureDef": CultureDef,
    "DamageArmorCategoryDef": DamageArmorCategoryDef,
    "DamageDef": DamageDef,
    "Def": Def,
    "DesignationCategoryDef": DesignationCategoryDef,
    "DesignationDef": DesignationDef,
    "DesignatorDropdownGroupDef": DesignatorDropdownGroupDef,
    "DifficultyDef": DifficultyDef,
    "DrugPolicyDef": DrugPolicyDef,
    "DutyDef": DutyDef,
    "EffecterDef": EffecterDef,
    "ExpansionDef": ExpansionDef,
    "ExpectationDef": ExpectationDef,
    "FactionDef": FactionDef,
    "FeatureDef": FeatureDef,
    "FleckDef": FleckDef,
    "FleshTypeDef": FleshTypeDef,
    "GameConditionDef": GameConditionDef,
    "GatheringDef": GatheringDef,
    "GauranlenTreeModeDef": GauranlenTreeModeDef,
    "GenStepDef": GenStepDef,
    "GoodwillSituationDef": GoodwillSituationDef,
    "HediffDef": HediffDef,
    "HediffGiverSetDef": HediffGiverSetDef,
    "HibernatableStateDef": HibernatableStateDef,
    "HistoryAutoRecorderDef": HistoryAutoRecorderDef,
    "HistoryAutoRecorderGroupDef": HistoryAutoRecorderGroupDef,
    "HistoryEventDef": HistoryEventDef,
    "IdeoFoundationDef": IdeoFoundationDef,
    "IdeoPresetCategoryDef": IdeoPresetCategoryDef,
    "IdeoPresetDef": IdeoPresetDef,
    "IdeoStoryPatternDef": IdeoStoryPatternDef,
    "IdeoSymbolPartDef": IdeoSymbolPartDef,
    "ImpactSoundTypeDef": ImpactSoundTypeDef,
    "ImplementOwnerTypeDef": ImplementOwnerTypeDef,
    "IncidentCategoryDef": IncidentCategoryDef,
    "IncidentDef": IncidentDef,
    "IncidentTargetTagDef": IncidentTargetTagDef,
    "InspirationDef": InspirationDef,
    "InstructionDef": InstructionDef,
    "InteractionDef": InteractionDef,
    "InventoryStockGroupDef": InventoryStockGroupDef,
    "IssueDef": IssueDef,
    "JobDef": JobDef,
    "JoyGiverDef": JoyGiverDef,
    "JoyKindDef": JoyKindDef,
    "KeyBindingCategoryDef": KeyBindingCategoryDef,
    "KeyBindingDef": KeyBindingDef,
    "LetterDef": LetterDef,
    "LifeStageDef": LifeStageDef,
    "LogEntryDef": LogEntryDef,
    "MainButtonDef": MainButtonDef,
    "ManeuverDef": ManeuverDef,
    "MapGeneratorDef": MapGeneratorDef,
    "MeditationFocusDef": MeditationFocusDef,
    "MemeDef": MemeDef,
    "MemeGroupDef": MemeGroupDef,
    "MentalBreakDef": MentalBreakDef,
    "MentalStateDef": MentalStateDef,
    "MessageTypeDef": MessageTypeDef,
    "NeedDef": NeedDef,
    "OrderedTakeGroupDef": OrderedTakeGroupDef,
    "PawnCapacityDef": PawnCapacityDef,
    "PawnColumnDef": PawnColumnDef,
    "PawnGroupKindDef": PawnGroupKindDef,
    "PawnKindDef": PawnKindDef,
    "PawnRelationDef": PawnRelationDef,
    "PawnsArrivalModeDef": PawnsArrivalModeDef,
    "PawnTableDef": PawnTableDef,
    "PlaceDef": PlaceDef,
    "PreceptDef": PreceptDef,
    "PrisonerInteractionModeDef": PrisonerInteractionModeDef,
    "QuestScriptDef": QuestScriptDef,
    "RaidStrategyDef": RaidStrategyDef,
    "RecipeDef": RecipeDef,
    "RecordDef": RecordDef,
    "ResearchProjectDef": ResearchProjectDef,
    "ResearchProjectTagDef": ResearchProjectTagDef,
    "ResearchTabDef": ResearchTabDef,
    "ReservationLayerDef": ReservationLayerDef,
    "RitualAttachableOutcomeEffectDef": RitualAttachableOutcomeEffectDef,
    "RitualBehaviorDef": RitualBehaviorDef,
    "RitualObligationTargetFilterDef": RitualObligationTargetFilterDef,
    "RitualOutcomeEffectDef": RitualOutcomeEffectDef,
    "RitualPatternDef": RitualPatternDef,
    "RitualTargetFilterDef": RitualTargetFilterDef,
    "RitualVisualEffectDef": RitualVisualEffectDef,
    "RiverDef": RiverDef,
    "RoadDef": RoadDef,
    "RoadPathingDef": RoadPathingDef,
    "RoadWorldLayerDef": RoadWorldLayerDef,
    "RoofDef": RoofDef,
    "RoomRoleDef": RoomRoleDef,
    "RoomStatDef": RoomStatDef,
    "RoyalTitleDef": RoyalTitleDef,
    "RoyalTitlePermitDef": RoyalTitlePermitDef,
    "RuleDef": RuleDef,
    "RulePackDef": RulePackDef,
    "ScatterableDef": ScatterableDef,
    "ScenarioDef": ScenarioDef,
    "ScenPartDef": ScenPartDef,
    "ShaderTypeDef": ShaderTypeDef,
    "ShipJobDef": ShipJobDef,
    "SitePartDef": SitePartDef,
    "SketchResolverDef": SketchResolverDef,
    "SkillDef": SkillDef,
    "SlaveInteractionModeDef": SlaveInteractionModeDef,
    "SongDef": SongDef,
    "SoundDef": SoundDef,
    "SpecialThingFilterDef": SpecialThingFilterDef,
    "StatCategoryDef": StatCategoryDef,
    "StatDef": StatDef,
    "StorytellerDef": StorytellerDef,
    "StuffAppearanceDef": StuffAppearanceDef,
    "StuffCategoryDef": StuffCategoryDef,
    "StyleCategoryDef": StyleCategoryDef,
    "StyleItemCategoryDef": StyleItemCategoryDef,
    "StyleItemDef": StyleItemDef,
    "SubcameraDef": SubcameraDef,
    "TaleDef": TaleDef,
    "TerrainAffordanceDef": TerrainAffordanceDef,
    "TerrainPatchMaker": TerrainPatchMaker,
    "TerrainThreshold": TerrainThreshold,
    "ThingCategoryDef": ThingCategoryDef,
    "ThingSetMakerDef": ThingSetMakerDef,
    "ThingStyleDef": ThingStyleDef,
    "ThinkTreeDef": ThinkTreeDef,
    "ThoughtDef": ThoughtDef,
    "TimeAssignmentDef": TimeAssignmentDef,
    "TipSetDef": TipSetDef,
    "ToolCapacityDef": ToolCapacityDef,
    "TraderKindDef": TraderKindDef,
    "TrainabilityDef": TrainabilityDef,
    "TrainableDef": TrainableDef,
    "TraitDef": TraitDef,
    "TransferableSorterDef": TransferableSorterDef,
    "TransportShipDef": TransportShipDef,
    "VerbProperties": VerbProperties,
    "WeaponClassDef": WeaponClassDef,
    "WeaponClassPairDef": WeaponClassPairDef,
    "WeaponTraitDef": WeaponTraitDef,
    "WeatherDef": WeatherDef,
    "WorkGiverDef": WorkGiverDef,
    "WorkGiverEquivalenceGroupDef": WorkGiverEquivalenceGroupDef,
    "WorkTypeDef": WorkTypeDef,
    "WorldGenStepDef": WorldGenStepDef,
    "WorldObjectDef": WorldObjectDef,
}
